﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CutTreeNew : MonoBehaviour {
	
	public enum Direction 
	{
		North, East, South, West
	}
	
	public Vector3 offsetVector;
	
	// Mistä suunnista puun voi katkoa
	public bool allowCutNorth;
	public bool allowCutEast;
	public bool allowCutSouth;
	public bool allowCutWest;
	
	private	Direction directionClass;

	private Vector3 treePos;
	private Vector3 mousePos;
	private Vector3 direction;
	
	static private Texture2D cursorNorth;
	static private Texture2D cursorEast;
	static private Texture2D cursorSouth;
	static private Texture2D cursorWest;
	
	private Vector2 cursorNorthOffset;
	private Vector2 cursorEastOffset;
	private Vector2 cursorSouthOffset;
	private Vector2 cursorWestOffset;
	
	public List<GameObject> obstacleNorth;	
	public List<GameObject> obstacleEast;	
	public List<GameObject> obstacleSouth;	
	public List<GameObject> obstacleWest;
	public GameObject obstacleSelf;
	
	private bool treeAlive;
	private float colliderRadius;
	
	private float baseXRotation;
	private float baseYRotation;
	private float baseZRotation;
	private float baseWRotation;
	
	private GameObject player;
	public float minDistanceToCut;
	
	private Vector3 basePosition;
	private Quaternion baseRotation;
	private float baseHeight;
	private float distanceToPlayer;
	// Use this for initialization
	void Start () {
		treeAlive = true;
		colliderRadius = gameObject.GetComponent<NavMeshObstacle>().radius;
		baseXRotation = gameObject.transform.rotation.x;
		baseYRotation = gameObject.transform.rotation.y;
		baseZRotation = gameObject.transform.rotation.z;
		baseWRotation = gameObject.transform.rotation.w;
		
		cursorNorthOffset = new Vector2(16.0f, 0.0f);
		cursorEastOffset = new Vector2(32.0f, 16.0f);
		cursorSouthOffset = new Vector2(-16.0f, 0.0f);
		cursorWestOffset = new Vector2(0.0f, 16.0f);
		if (cursorNorth == null)
		{
			cursorNorth = (Texture2D)Resources.Load("saw_up", typeof(Texture2D));
		}
		if (cursorEast == null)
		{
			cursorEast = (Texture2D)Resources.Load("saw_right", typeof(Texture2D));
		}
		if (cursorSouth == null)
		{
			cursorSouth = (Texture2D)Resources.Load("saw_down", typeof(Texture2D));
		}
		if (cursorWest == null)
		{
			cursorWest = (Texture2D)Resources.Load("saw_left", typeof(Texture2D));
		}
		player = GameObject.FindGameObjectWithTag("Player");
		foreach (var obj in GameObject.FindGameObjectsWithTag("Player"))
		{
			Debug.Log (obj.name);
		}
		minDistanceToCut = 50.0f;
		basePosition = this.transform.position;
		baseRotation = this.transform.rotation;
		baseHeight = basePosition.y;
		treeAlive = true;
	}
	
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void Respawn () {
		treeAlive = true;
		gameObject.GetComponent<NavMeshObstacle>().radius = colliderRadius;
		gameObject.transform.rotation.Set(baseXRotation, baseYRotation, baseZRotation, baseWRotation);
		Debug.Log("Respawn called");
	}
	
	void OnMouseOver () {
		//TODO: todella oksettavaa purkkaa, korjaa myöhemmin
        if (TheGame.Instance.CurrentState == TheGame.State.GAMEPLAY)
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            // Lasketaan suunta kursorista puun keskelle
            treePos = this.collider.transform.position + offsetVector;

            direction = -1 * treePos + mousePos;


            Debug.DrawRay(treePos, direction, Color.red, 0.1f, false);
			if (treeAlive)
			{
            	directionClass = ClassifyOrientation(direction);
			}

            if (Input.GetMouseButton(0))
            {
                CutTree(directionClass);
            }
        }
	}
	
	void OnMouseExit () {
		Cursor.SetCursor(null, Vector2.zero,CursorMode.Auto);
	}
	
	Direction ClassifyOrientation(Vector3 direction)
	{
		Direction retVal = Direction.North;
		if (Mathf.Abs(direction.x) < Mathf.Abs (direction.z))
		{
			if (direction.z < 0.0f)
			{
				retVal = Direction.North;
				if (allowCutNorth)
				{
					Cursor.SetCursor(cursorNorth, cursorNorthOffset, CursorMode.Auto);
				}
			}
			else
			{
				retVal = Direction.South;
				if (allowCutSouth)
				{
					Cursor.SetCursor(cursorSouth, cursorSouthOffset, CursorMode.Auto);
				}
			}
		}
		else
		{
			if (direction.x < 0.0f)
			{
				retVal = Direction.East;
				if (allowCutEast)
				{
					Cursor.SetCursor(cursorEast, cursorEastOffset, CursorMode.Auto);
				}
			}
			else
			{
				retVal = Direction.West;
				if (allowCutWest)
				{
					Cursor.SetCursor(cursorWest, cursorWestOffset, CursorMode.Auto);
				}
			}	
		}
		return retVal;
	}
	
	public void CutTree(Direction direction)
	{
		distanceToPlayer = Vector3.Distance(this.transform.position, player.transform.position);
		if (treeAlive && distanceToPlayer < minDistanceToCut)
		{
			Debug.Log ("Tänne meni");

			//gameObject.GetComponent<NavMeshObstacle>().radius = 0.0f;
			obstacleSelf.transform.Translate(new Vector3(0.0f, 100.0f, 0.0f), Space.World);
			
			if (direction == Direction.North && allowCutNorth)
			{
				// Kaadetaan puu
				Animations.AnimationManager.PlayAnimation(player, Animations.PigAnimations.Cut);
				
				this.transform.Rotate(0, -90, 0);
				this.transform.Translate(new Vector3(0.0f, -18.0f, 18.0f), Space.World);

				foreach (GameObject obstacle in obstacleNorth)
				{
					obstacle.GetComponent<NavMeshObstacle>().transform.Translate(new Vector3(0.0f, 100.0f, 0.0f), Space.World);
                    destroyTree();
				}
				
				treeAlive = false;
			}
			else if (direction == Direction.East && allowCutEast)
			{
				
				Animations.AnimationManager.PlayAnimation(player, Animations.PigAnimations.Cut);
				
				this.transform.Rotate(90, 0, 0);
				this.transform.Translate(new Vector3(18.0f, -18.0f, 0.0f), Space.World);
			
				foreach (GameObject obstacle in obstacleEast)
				{
					obstacle.GetComponent<NavMeshObstacle>().transform.Translate(new Vector3(0.0f, 100.0f, 0.0f), Space.World);
                    destroyTree();
				}
				
				treeAlive = false;
			}
			else if (direction == Direction.South && allowCutSouth)
			{
				Animations.AnimationManager.PlayAnimation(player, Animations.PigAnimations.Cut);
				
				this.transform.Rotate(0, 90, 0);
				this.transform.Translate(new Vector3(0.0f, -18.0f, -18.0f), Space.World);
				
				// Tuhotaan este
				foreach (GameObject obstacle in obstacleSouth)
				{
					obstacle.GetComponent<NavMeshObstacle>().transform.Translate(new Vector3(0.0f, 100.0f, 0.0f), Space.World);
                    destroyTree();

				}
				
				treeAlive = false;
				
			}
			else if (direction == Direction.West && allowCutWest)
			{
				Animations.AnimationManager.PlayAnimation(player, Animations.PigAnimations.Cut);
				
				this.transform.Rotate(-90, 0, 0);
				this.transform.Translate(new Vector3(-18.0f, -18.0f, 0.0f), Space.World);
				
				foreach (GameObject obstacle in obstacleWest)
				{				
					obstacle.GetComponent<NavMeshObstacle>().transform.Translate(new Vector3(0.0f, 100.0f, 0.0f), Space.World);
                    destroyTree();
                    
				}
				Animations.AnimationManager.PlayAnimation(player, Animations.PigAnimations.Cut);
				treeAlive = false;
                
			}
			
			Debug.Log (player.GetComponent<Animator>().GetInteger("Animation"));
			this.enabled = false;
		}
		else // Mennään puun viereen
		{
			Debug.Log ("Annnetaan kohde");
			
			Vector3 vector = this.transform.position + Vector3.down.normalized * this.transform.position.y;
            Vector3 vectorOffset = new Vector3(0, 0, 0);
            float offsetValue = 30.0F;
            if (direction == Direction.North) vectorOffset.z = -offsetValue;
            else if (direction == Direction.South) vectorOffset.z = offsetValue;
            else if (direction == Direction.East) vectorOffset.x = -offsetValue;
            else if (direction == Direction.West) vectorOffset.x = offsetValue;
			//Vector3 vectorOffset = this.transform.position - player.transform.position;
			//vectorOffset.Normalize();
			//vectorOffset = vectorOffset * -30.0f;
            player.GetComponent<MovingWithNavMeshOrtho>().SetDestinationTree(vector + vectorOffset, this.gameObject, direction);			
		}
	
	}
	public void ResetTree()
	{
		//Debug.Log ("ResetTree");
		this.transform.position = basePosition;
		this.transform.rotation = baseRotation;
		treeAlive = true;
		foreach (GameObject obj in obstacleNorth)
		{
			if (obj != null)
			{
				obj.transform.position = new Vector3(obj.transform.position.x, baseHeight, obj.transform.position.z);
			}
		}
		foreach (GameObject obj in obstacleEast)
		{
			if (obj != null)
			{
				obj.transform.position = new Vector3(obj.transform.position.x, baseHeight, obj.transform.position.z);
			}
		}
		foreach (GameObject obj in obstacleSouth)
		{
			if (obj != null)
			{
				obj.transform.position = new Vector3(obj.transform.position.x, baseHeight, obj.transform.position.z);
			}
		}
		foreach (GameObject obj in obstacleWest)
		{
			if (obj != null)
			{
				obj.transform.position = new Vector3(obj.transform.position.x, baseHeight, obj.transform.position.z);
			}
		}
	}

    private void destroyTree()
    {
        GameObject.Find("SceneDirector").GetComponent<CutTreePuzzleSceneDirector>().TreeCut();
		GameObject.Find("Sound-Chainsaw").GetComponent<Sound.PlaySound>().PlaySoundOneTime();

        Debug.Log("Destroying Tree");
    }
}
