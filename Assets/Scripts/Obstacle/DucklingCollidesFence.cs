﻿using UnityEngine;
using System.Collections;

public class DucklingCollidesFence : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other)
	{
		Debug.Log ("Collides with: " + other.tag);
		if(other.tag == "Duckling")
		{
            other.gameObject.GetComponent<FollowNew>().GoToFinish();
		}
	}
}
