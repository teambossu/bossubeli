﻿using UnityEngine;
using System.Collections;

public class LightController : MonoBehaviour {

    public float DimmingSpeed = 0.2F;
    private float m_maxIntensity = 0.79F;

    private bool m_dimming = false;
    private bool m_brightening = false;

    Light m_light;
	// Use this for initialization
	void Start () {

        m_light = gameObject.GetComponent<Light>();
	
	}
	
	// Update is called once per frame
	void Update () {

        if (m_dimming)
        {
            if (m_light.intensity > 0)
            {
                m_light.intensity -= DimmingSpeed * Time.deltaTime;
            }
            else
            {
                m_light.intensity = 0;
                TheGame.Instance.endPauseState();
                m_dimming = false;
            }
        }
        else if (m_brightening)
        {
            Debug.Log("LightController: brightening, original value: " + m_light.intensity);
            if (m_light.intensity < m_maxIntensity)
            {
                if (m_light.intensity == 0)
                {
                    m_light.intensity = 0.01F;
                }
                else
                {
               
                    m_light.intensity += DimmingSpeed * Time.deltaTime;
                }
            }
            else
            {
                m_light.intensity = m_maxIntensity;
                TheGame.Instance.endPauseState();
                m_brightening = false;
            }
        }
	
	}

    public void FadeOut()
    {
        Debug.Log("LightController: beginning FadeOut");
        TheGame.Instance.initializePauseState();
        m_dimming = true;
    }

    public void FadeIn()
    {
        Debug.Log("LightController: beginning FadeIn");
        TheGame.Instance.initializePauseState();
        m_brightening = true;
    }

    public void TurnOff()
    {
        gameObject.GetComponent<Light>().intensity = 0;
    }
}
