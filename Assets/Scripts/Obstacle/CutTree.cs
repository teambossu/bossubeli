﻿using UnityEngine;
using System.Collections;

namespace CutTree
{
	public enum MouseOverAction
	{
		Enter = 0,
		Exit = 1
	}
	public delegate void MouseOverEventHandler(object sender, MouseOverAction action);
	
	public class CutTree : MonoBehaviour {
		
		public AudioClip sound;
		public Texture2D hoverCursor; // Kursori joka näytetään kun pidetään hiirtä hahmon yläpuolella.
		public Texture2D rightCursor;
		public Vector2 cursorAdjustment = new Vector2(10.0f, 10.0f); // Tällä voidaan muuttaa mihin kursori osoittaa
		public event MouseOverEventHandler mouseOverEvent; // Hiirellä osoitus eventti
		
		
	// This is the object to follow
	private Transform player;
	// This is the object which follows
	//public Transform tree;
	public GameObject bridge;
	// This is used to store the distance between the two objects.
	private float range;
	private float angle;
	public float wantedrange;
		// Use this for initialization
		void Start () 
		{
			player = GameObject.FindGameObjectWithTag("Player").transform;
			Debug.Log ("CutTree: player asetettu" + player);
		}
		
		// Update is called once per frame
		void Update () {
		}
		
		// Tämä on MonoBehaviourin tarjoama rajapinta funktio jota kutsutaan kun hiiri osoittaa Collider componenttiin (esim. Box Collider)
		void OnMouseOver() {
			
			//siirretään mouseposition niin että treeposition on origo
			Vector3 relativeMouseposition = Input.mousePosition - transform.position;
			
			//siirretään kaksiulotteiseen koordinaatistoon (xz)
			Vector2 mouseposition2D = new Vector2(relativeMouseposition.x, relativeMouseposition.z);
			//nolla-asteen vektori
			Vector2 zeroPosition = new Vector2(0,1);
			
			float mouseAngle = Vector2.Angle (zeroPosition, mouseposition2D);
			
			//targetDir on vector playeristä puuhun			
			Vector3 targetDir = player.position - transform.position;
			
			Debug.DrawRay(this.transform.position, targetDir, Color.red, 2.0f);
			//range kertoo playerin ja puun välisen etäisyyden
			range = Vector3.Distance( player.position, transform.position );
			//angle laskee kulman
			angle = Vector3.Angle ( player.forward , targetDir);
			
			
			//määritellään missä kulmassa näytetään cursoria
			//ja kuinka kaukaa näytetään
			if( range < wantedrange && angle > 150 )
			{
			// Jos on kuuntelijoita
			if (mouseOverEvent != null) {
				// Kerrotaan kuuntelijoille että hiiri osoittaa nyt objektia
				mouseOverEvent(this, MouseOverAction.Enter);
			}
			
			// Jos on annettu vaihdettava kursori
			if (hoverCursor != null)
			{
				// Annettaan käyttöjärjestelmälle piirrettävä kursori
				Cursor.SetCursor(hoverCursor, cursorAdjustment, CursorMode.Auto);
			}
			// Jos painetaan hiiren vasenta näppäintä niin
			if (Input.GetMouseButtonDown(0))
			{
				//clickitem
				Debug.Log ("Click Item!");
				//soitetaan ääni
				audio.PlayOneShot(sound);
				
				
				Vector3 temp = player.position - transform.position;
				transform.rotation = Quaternion.LookRotation(temp);
				transform.Translate(0, 4 , 0);
				Debug.Log ("MouseAngle:" + mouseAngle);
				bridge.SetActive(true);
				//Debug.Log (angle);
			}
			}
		}
		// Tämä on MonoBehaviourin tarjoama rajapinta funktio jota kutsutaan kun hiiri ei enää osoittaa Collider componenttiin (esim. Box Collider)
		 void OnMouseExit() {
			
			// Jos on kuuntelijoita
			if (mouseOverEvent != null)
			{
				// Kerrotaan että hiiri ei enää osoita objektia
				mouseOverEvent(this, MouseOverAction.Exit);
			}
			
			// Käsketään käyttöjärjestelmän piirtää vakio kursori.
			Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
		}
		
	}
}