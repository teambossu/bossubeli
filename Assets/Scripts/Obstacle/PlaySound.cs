﻿using UnityEngine;
using System.Collections;

namespace Sound
{

public class PlaySound : MonoBehaviour {
	
	public AudioClip clip;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
	
	public void PlaySoundOneTime()
	{
		audio.PlayOneShot(clip);
	}
}
}