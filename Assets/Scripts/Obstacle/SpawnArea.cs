﻿using UnityEngine;
using System.Collections;

public class SpawnArea : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	
	void OnTriggerEnter(Collider other)
	{
		Debug.Log("Collider other: " + other.tag);
		if(other.gameObject.tag == "Duckling")
		{
			other.gameObject.GetComponent<FollowNew>().Spawn();
		}
	}
}