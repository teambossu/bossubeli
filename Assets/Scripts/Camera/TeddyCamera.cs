﻿using UnityEngine;
using System.Collections;

public class TeddyCamera : MonoBehaviour
{


    private float targetX = 173;
    private float targetZ = 700;
    private float homeX;
    private float homeZ;
    public float MovingSpeed;
    private bool movingToTarget = false;
    private bool movingBack = false;
    // Use this for initialization
    void Start()
    {
        homeX = transform.position.x;
        homeZ = transform.position.z;

    }

    // Update is called once per frame
    void Update()
    {
        if (movingToTarget)
        {


            bool Xthere = false;
            bool Zthere = false;
            Vector3 newPosition = transform.position;

            if (newPosition.x > targetX) newPosition.x -= MovingSpeed * Time.deltaTime;
            else Xthere = true;

            if (newPosition.z < targetZ) newPosition.z += MovingSpeed * Time.deltaTime;
            else Zthere = true;

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                newPosition.x = targetX;
                newPosition.z = targetZ;
                Xthere = true;
                Zthere = true;
            }

            transform.position = newPosition;

            if (Xthere && Zthere)
            {
                movingToTarget = false;
                ActionManager.Instance.doAction("TeddyCameraThere");
            }
        }

    }

    public void MoveToTeddy()
    {
        CameraManager.Instance.SwitchToCamera("CAM_TEDDY");
        TheGame.Instance.initializePauseState();
        movingToTarget = true;
    }

    public void MoveBack()
    {
        movingBack = false;
        TheGame.Instance.endPauseState();
        CameraManager.Instance.SwitchToCamera("CAM_MAIN");

    }

}