﻿using UnityEngine;
using System.Collections;

public class AreaCam : MonoBehaviour {
		
	public Camera camera;
	
	// Use this for initialization
	void Start () 
	{
		Debug.Log("Start AreaCam");
		camera.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other)
	{
		Debug.Log("OnTriggerEnter AreaCam");
		camera.enabled = true;
    
    }
	
	void OnTriggerExit (Collider other) 
	{
		Debug.Log("OnTriggerExit AreaCam");
		camera.enabled = false;
	}
}
