﻿using UnityEngine;
using System.Collections;

public class MovingWithNavMeshOrtho: MonoBehaviour {
	
	// Vakiot
	private const int MOUSE_LEFT = 0;				// Hiiren vasemman näppäimen keycode on 0
	private const string TERRAIN = "Terrain";		// Liikuttava maasto TAG:ätään Terrainiksi
	private const string ITEM = "Item";
    private const string CHARACTER = "Character";
	private const string SCENECHANGER = "SceneChanger";
    private const string TREE = "Tree";
	private const float ROTATION_THRESHOLD = 10.0f;	// Kuinka tarkasti GameObjectin tulee kääntyä kohteeseen
	private const string CAMERAAREA = "CameraArea";
	// Privatet
	private Transform transform; // GameObjectin johon tämä skripti asetetaan muutosmatriisi.
	public Vector3 destination; // Määränpää johon GameObject on matkalla.
	private float distance;		 // Etäisyys kohteeseen jotta voidaan laskea nopeus.
	private Camera camera;
	private Animator animator;
	private NavMeshAgent agent;

    //save here the character we are moving towards if we are moving closer to talk
    public GameObject walkingTowards;
    private CutTreeNew.Direction cutDirection;
	
	// Use this for initialization
	void Start () {
		transform = gameObject.transform;	// Otetaan gameObjectin muutosmatriisi
        destination = transform.position;			// Asetetaan kohde tyhjäksi jotta ei alussa liikua
		animator = GetComponent<Animator>();
		agent = (NavMeshAgent)this.GetComponent("NavMeshAgent");
	}
	
	// Update is called once per frame
	void Update () {
		
		distance = Vector3.Distance (destination, transform.position);
		
		
        //katsotaan, saavuttiinko perille hahmon luo mitä ollaan aiemmin klikattu
        if (distance < 3.0f && walkingTowards != null)
        {
            //siirretään possu puhumispisteeseen, käännetään osoittamaan kohti puhuttavaa ja aloitetaan keskustelu
            transform.position = destination;
            transform.LookAt(walkingTowards.transform);
            
			if(walkingTowards.tag == CHARACTER)
			{
           	 walkingTowards.GetComponent<Talkable.Talkable>().StartConversation();
			}
			else if(walkingTowards.tag == SCENECHANGER)
			{
				walkingTowards.GetComponent<ChangeScene.ChangeScene>().ChangeSceneTo();
			}
            else if(walkingTowards.tag == TREE)
            {
                walkingTowards.GetComponent<CutTreeNew>().CutTree(cutDirection);
            }
            //nollataan liikkumiseen käytetyt muuttujat
            destination = transform.position;
            walkingTowards = null;
        }

		if (agent != null)
		{
			//Debug.Log (animator.GetInteger("Animation"));
			if (!agent.hasPath && animator.GetInteger("Animation") != 2)
			{
				Animations.AnimationManager.PlayAnimation(this.gameObject, Animations.PigAnimations.Idle);
			}
		}
		// Jos käyttäjä klikkaa vasemmalla hiiren painikkeella niin katsotaan osuiko hän kuljettavaan pintaan (jos ollaan GAMEPLAY -tilassa)
		if (Input.GetMouseButtonDown(MOUSE_LEFT) && TheGame.Instance.CurrentState == TheGame.State.GAMEPLAY)
		{
            
			RaycastHit objectHit;
			Ray rayCast = Camera.main.ScreenPointToRay(Input.mousePosition);
			// Ammutaan säde kamerasta kursoriin
			if (Physics.Raycast (rayCast, out objectHit))
			{
			 // Jos osui johonkin niin tarkastetaan onko se käveltävä pinta.

				if (objectHit.collider.gameObject.tag == TERRAIN || objectHit.collider.gameObject.tag == ITEM
					|| objectHit.collider.gameObject.tag == CAMERAAREA )
				{
                    // Nollataan hahmo, jota kohti oltiin kävelemässä
                    walkingTowards = null;
					// Jos pinta oli käveltävää niin asetetaan kohdepiste.
					destination = rayCast.GetPoint(Vector3.Distance(objectHit.point, Camera.main.transform.position));
					destination.y = 0.0f;
					// Debug tulostusta
					
					if (agent != null)
					{
						Animations.AnimationManager.PlayAnimation(this.gameObject, Animations.PigAnimations.Walk);
						agent.destination = destination;
					}
					
				}
                
                else if (objectHit.collider.gameObject.tag == CHARACTER || objectHit.collider.gameObject.tag == SCENECHANGER)
                {
					Debug.Log ("ObjectHit: " + objectHit.collider.gameObject.tag );
					
					if(objectHit.collider.gameObject.tag == CHARACTER)
					{
                    	destination = objectHit.collider.gameObject.GetComponent<Talkable.Talkable>().OnClick();
					}
					else if (objectHit.collider.gameObject.tag == SCENECHANGER)
					{
						destination = objectHit.collider.gameObject.GetComponent<ChangeScene.ChangeScene>().OnClick();
					}
                    walkingTowards = objectHit.collider.gameObject;


                    if (agent != null)
                    {
                        Animations.AnimationManager.PlayAnimation(this.gameObject, Animations.PigAnimations.Walk);
                        agent.destination = destination;
                    }

                }
                     
				
			}
		}
		
		if (destination != null && distance > 3.0f)
		{
			agent.destination = destination;
		}
	}
	
	public void SetDestination(Vector3 dest, GameObject wlkTwrds)
	{
		destination = dest;
		walkingTowards = wlkTwrds;
	}

    public void SetDestinationTree(Vector3 dest, GameObject wlkTowards, CutTreeNew.Direction dir)
    {
        destination = dest;
        walkingTowards = wlkTowards;
        cutDirection = dir;
    }
    public void Stop()
    {
        destination = GameObject.FindGameObjectWithTag("Player").transform.position;
    }
}
