﻿using UnityEngine;
using System.Collections;

public class PickItem : MonoBehaviour {
	
		
public AudioClip pickItem;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other) {
		
        audio.PlayOneShot(pickItem);
        Destroy(other.gameObject);
    }
}
