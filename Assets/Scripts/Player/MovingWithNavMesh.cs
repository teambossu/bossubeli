﻿using UnityEngine;
using System.Collections;

public class MovingWithNavMesh: MonoBehaviour {
	
	// Vakiot
	private const int MOUSE_LEFT = 0;				// Hiiren vasemman näppäimen keycode on 0
	private const string TERRAIN = "Terrain";		// Liikuttava maasto TAG:ätään Terrainiksi
	private const string ITEM = "Item";
    private const string CHARACTER = "Character";
	private const string SCENECHANGER = "SceneChanger";
    private const string ENTRYPOINT = "EntryPoint";
	private const float ROTATION_THRESHOLD = 10.0f;	// Kuinka tarkasti GameObjectin tulee kääntyä kohteeseen
	private const string CAMERAAREA = "CameraArea";
	// Privatet
	private Transform transform; // GameObjectin johon tämä skripti asetetaan muutosmatriisi.
	private Vector3 destination; // Määränpää johon GameObject on matkalla.
	private float distance;		 // Etäisyys kohteeseen jotta voidaan laskea nopeus.
	private Camera camera;
	private Animator animator;
	private NavMeshAgent agent;
	public GameObject pigWithChainsaw;
	private Animator animatorChainsaw;

    //save here the character we are moving towards if we are moving closer to talk
    private GameObject walkingTowards;
	
	// Use this for initialization
	void Start () {
		transform = gameObject.transform;	// Otetaan gameObjectin muutosmatriisi
		destination = Vector3.zero;			// Asetetaan kohde tyhjäksi jotta ei alussa liikua
		animator = GetComponent<Animator>();
		agent = (NavMeshAgent)this.GetComponent("NavMeshAgent");
		if (pigWithChainsaw != null)
		{
			animatorChainsaw = pigWithChainsaw.GetComponent<Animator>();
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		
        
		distance = Vector3.Distance (destination, transform.position);
		
		
        //katsotaan, saavuttiinko perille hahmon luo mitä ollaan aiemmin klikattu
        if (distance < 3.0f && walkingTowards != null)
        {
            Debug.Log("MovingWithNavMesh: Arrived at " + walkingTowards.name);
            //vaihdetaan päätepiste nykyiseksi pisteeksi
            destination = gameObject.transform.position;
            //stop the agent and stop the walking animation
            agent.Stop(true);
            animator.SetBool("Walking", false);
            
            
            
            
			if(walkingTowards.tag == CHARACTER || walkingTowards.tag == ENTRYPOINT)
			{
                //siirretään possu puhumispisteeseen, käännetään osoittamaan kohti puhuttavaa ja aloitetaan keskustelu
                transform.position = destination;
                Vector3 lookTarget = walkingTowards.transform.position;
                //poistetaan y-komponentti jotta hahmo ei kallistu
                lookTarget.y = 0;
                transform.LookAt(lookTarget);     
                Debug.Log(string.Format("MovingWithNavMesh Turning to look at: x: {0}, y: {1}, z: {2}",
                                                walkingTowards.transform.localPosition.x,
                                                walkingTowards.transform.localPosition.y,
                                                walkingTowards.transform.localPosition.z));
           	    walkingTowards.GetComponent<Talkable.Talkable>().StartConversation();
			}
			else if(walkingTowards.tag == SCENECHANGER)
			{
				walkingTowards.GetComponent<ChangeScene.ChangeScene>().ChangeSceneTo();
			}
            //nollataan liikkumiseen käytetyt muuttujat
            walkingTowards = null;
        }

		if (agent != null)
		{
			if (!agent.hasPath)
			{
				animator.SetBool ("Walking", false);
				if (animatorChainsaw != null)
				{
					animatorChainsaw.SetInteger("Animation", 0);
				}
			}
		}
		// Jos käyttäjä klikkaa vasemmalla hiiren painikkeella niin katsotaan osuiko hän kuljettavaan pintaan (jos ollaan GAMEPLAY -tilassa)
		if (Input.GetMouseButtonDown(MOUSE_LEFT) && TheGame.Instance.CurrentState == TheGame.State.GAMEPLAY)
		{
            agent.Resume();
			RaycastHit objectHit;
			Ray rayCast = Camera.main.ScreenPointToRay(Input.mousePosition);
			// Ammutaan säde kamerasta kursoriin
			if (Physics.Raycast (rayCast, out objectHit))
			{
			 // Jos osui johonkin niin tarkastetaan onko se käveltävä pinta.
				if (objectHit.collider.gameObject.tag == TERRAIN || objectHit.collider.gameObject.tag == ITEM
					|| objectHit.collider.gameObject.tag == CAMERAAREA )
				{
                    // Nollataan hahmo, jota kohti oltiin kävelemässä
                    walkingTowards = null;
					// Jos pinta oli käveltävää niin asetetaan kohdepiste.
					destination = rayCast.GetPoint(Vector3.Distance(objectHit.point, Camera.main.transform.position));
					
					// Debug tulostusta
					Debug.Log(string.Format("LeftClick World Pos: x: {0}, y: {1}, z: {2}", 
											objectHit.point.x, 
											objectHit.point.y, 
											objectHit.point.z));
					
					if (agent != null)
					{
						animator.SetBool ("Walking", true);
						if (animatorChainsaw != null)
						{
							animatorChainsaw.SetInteger("Animation", 1);
						}
						agent.destination = destination;
					}
					
				}
                
                else if (objectHit.collider.gameObject.CompareTag(CHARACTER) || objectHit.collider.gameObject.tag == SCENECHANGER || objectHit.collider.gameObject.tag == ENTRYPOINT)
                {
					Debug.Log ("ObjectHit: " + objectHit.collider.gameObject.tag );
					
					if(objectHit.collider.gameObject.tag == CHARACTER || objectHit.collider.gameObject.tag == ENTRYPOINT)
					{
                        
                    	destination = objectHit.collider.gameObject.GetComponent<Talkable.Talkable>().OnClick();
					}
					else if (objectHit.collider.gameObject.tag == SCENECHANGER)
					{
						destination = objectHit.collider.gameObject.GetComponent<ChangeScene.ChangeScene>().OnClick();
					}
                    walkingTowards = objectHit.collider.gameObject;

                    Debug.Log(string.Format("LeftClick Character, Talking Position: x: {0}, y: {1}, z: {2}",
                                            destination.x,
                                            destination.y,
                                            destination.z));

                    if (agent != null)
                    {
                        animator.SetBool("Walking", true);
						if (animatorChainsaw != null)
						{
							animatorChainsaw.SetInteger("Animation", 1);
						}
                        agent.destination = destination;
                    }

                }
                     
				
			}
		}
		
				
			
			
			
				

			
		
		
	}
}
