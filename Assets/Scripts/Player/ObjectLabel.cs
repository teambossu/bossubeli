﻿/*
 * Object label on GUIText komponentti joka voidaan yhdistää gameobjektiin
 * jolla on Talkable scripti yhdistettynä. Tällöin teksti näytetään kun gameobjectin päälle
 * siirretään hiiri ja piiloitetaan kun hiiri poistuu hahmon päältä.
 * - Tomi
 */


using UnityEngine;
using System.Collections;
using Talkable;

[RequireComponent(typeof(GUIText))]
public class ObjectLabel : MonoBehaviour {
	
	public GameObject listenTo; // GameObjecti jonka mouseOveria seurataan.
	
	private Talkable.Talkable talkable; // Tähän tallentuu talkable olio joka hoitaa mouseOver tarkistelun.

	void Start () {
				
		guiText.enabled = false; // Piilotetaan nimi alussa
		
		// Jos ei ole mitään seurattavaa niin ei yritetä seurata.		
		if (listenTo != null)
		{
			// Jos seurattavalla ei ole talkable componenttia niin ei voida seurata			
			if (listenTo.GetComponent("Talkable") != null)
			{
				// Haetaan seurattavalta peliobjektilta talkable componentti
				talkable = (Talkable.Talkable)listenTo.GetComponent("Talkable"); 
				// Ilmoitetaan komponentille että halutaan että kutsutaan OnTalkableEvent
				// funktiota kun hiiri osuu seurattavaan peliobjektiin.
				talkable.mouseOverEvent += new MouseOverEventHandler(OnTalkableEvent);
			}
		}		
	}
	
	void Update () {
	}
	
	// Talkable object kutsuu tätä funktiota kun hiiri osuu peliobjektiin.
	private void OnTalkableEvent(object sender, Talkable.MouseOverAction action)
	{
		// Jos hiiri osoittaa peliobjektia näytetään label
		if (action == MouseOverAction.Enter)
		{
			guiText.enabled = true;	
		}
		// Jos ei niin ei näytetä
		else
		{
			guiText.enabled = false;
		}
		
	}
	
}
