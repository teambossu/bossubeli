﻿


var UISkin : GUISkin;
var UISkinAltSkin : GUISkin;
var BackpackOpen : boolean;
var TalkBoxOpen : boolean;
var chatWheelOpen : boolean;
var btnWidth :int  = 100;
var btnHeight :int = 100;
var spacing :int = 10;
var conversationID = "";

private var ConversationCounter : int;
private var CLength : int;
private var conversationText = "";
var conversationKeyWordSpot : int = 1; //-1 jos keskustelussa ei ole avainsanaa, alkaa 0:sta
var conversationKeyWord = "english";
var keyWordAnimationPos : int = 0;
var keyWordClicked : boolean = false;

var wordSpacing : int = 10; //Space between words
var LetterSize : int = 25; //Size of 1 letter  (for dialog)  //Fontsizen pitäis olla näillä asetuksilla 40 ja font cour.



var BackpackItems = new Array(); //Backpackin tavarat. Tulee jostain muualta?
var ConversationLines = new Array(); //Lines of conversation
var chatWheelImages = new Array(); //Chatwheelin kuvat

var imageForChatWheel : Texture2D; //Purkka, myöhemmin kuva tulee jostain managerilta.
var debugInt : int = 85; //Graffojen paikalle laittamiseen

ConversationLines.push("Mr Racoon I assume?");
ConversationLines.push("Mi no habla english");
ConversationLines.push("Crap...");


//var icon : Texture2D;  Kuva voidaan laittaa GUI-objectiin txt-contentin päälle




function Update()
{
	if(keyWordClicked)
	{
		keyWordAnimationPos += (Time.deltaTime * 800.0);
	}
	
	if(keyWordAnimationPos > 700)
	{
						chatWheelImages.push(imageForChatWheel);
						chatWheelOpen = true;
						ConversationLines = [];
						ConversationCounter = 0;
						keyWordClicked = false;
						keyWordAnimationPos = 0;
	}
}


function OnGUI()
{


	// Assign the skin to be the one currently used.
	GUI.skin = UISkin;
	
	
	
	
	if(GUI.Button(Rect(10,10, btnWidth,btnHeight), "To Menu"))
	{
		Application.LoadLevel("MenuScene");
	}
	
	if(GUI.Button(Rect(btnWidth + spacing + 10,10, btnWidth,btnHeight), "Exit"))
	{
			Application.Quit();

	}
	
	if((GUI.Button(Rect(10,10 + btnHeight + spacing, btnWidth,btnHeight), "Conv")))
	{
		if(!TalkBoxOpen)
		{
		TalkBoxOpen = true;
		}
		else
		{
		TalkBoxOpen = false;
		}
	}
	
	if((GUI.Button(Rect((btnWidth) + spacing + 10,10 + btnHeight + spacing, btnWidth,btnHeight), "Talk")))
	{
		if(TalkBoxOpen)
		{
		  //Tähän kutsut conversation managerille
		  //ConversationLines on array jossa säilötään keskustelu repliikit stringeinä
		  //
		  
		}
	}
	
	if(GUI.Button(Rect((btnWidth * 2) + (spacing *2) + 10, 10, btnWidth, btnHeight), "Bag"))
	{
		if(!BackpackOpen)
		{
		BackpackOpen = true;
		}
		else
		{
		BackpackOpen = false;
		}
	}
	
	if(BackpackOpen)
	{
		GUI.BeginGroup(Rect((btnWidth * 3) + (spacing * 3) + 10, 10, (Screen.width - ((btnWidth * 3) + (spacing *2))) - 30, 500));
		GUI.Box(Rect(0,0, (Screen.width - ((btnWidth * 5) + (spacing *2) - 50)) , 300),"");
		
		for (var i = 0; i < 6; i++)
		{
			GUI.Button(Rect(30 + i*100,30, 100, 100), "item" + i);
			
		}
		GUI.EndGroup();
			
	}
	
	if(TalkBoxOpen)
	{
	
	
	GUI.BeginGroup(Rect(10, Screen.height*3 / 4, Screen.width - 20, Screen.height / 4 - 10 ));
	GUI.Box(Rect(20,0, Screen.width * 3 / 4 - 20, Screen.height / 4 - 10), "");
	
	//ChatWheel
	if(chatWheelOpen)
	{
		GUI.Box(Rect(Screen.width * 3 / 4 + 10, 0, Screen.width / 4 - 20, Screen.height / 4 - 10), "");
		if(chatWheelImages.length != 0)
		{
			var image : Texture2D = chatWheelImages[0];
			GUI.skin = null;
			if (GUI.Button(Rect( Screen.width * 3 / 4 + debugInt, 40, Screen.width / 8, Screen.height / 4 - 90), image))
			{
			
				ConversationLines = [];
				ConversationCounter = 0;
				ConversationLines.push("English is not english in spanish");
				ConversationLines.push("Caca de toro, you got me!");
				ConversationLines.push("So will you talk to me now?");
				ConversationLines.push("No.");
				ConversationLines.push("Crap...");
			}
			
			GUI.skin = UISkin;
		}
		
		
	}
	
	CLength = ConversationLines.length; //Keskustelun pituus
	
	if(CLength != 0) //Jos keskustelu on olemassa
	{
		conversationText = ConversationLines[ConversationCounter]; //Otetaan keskusteluLine talteen
	
		var wordBuffer = conversationText.Split(" "[0]); //Pätkitään line string arrayksi
		var previousButtonSpace : int = 0; //Kuinka paljon edellinen sana vei tilaa.
		
		for (var j = 0; j < wordBuffer.length; j++)
		{
			var wordLength : int = wordBuffer[j].length;
			var spot : int = 0;
			
			if(ConversationCounter == conversationKeyWordSpot)
			{
				if(wordBuffer[j] == conversationKeyWord)
				{
						spot = 1; //Purkkaaa, jos on spotti nii sit animaatiolisä X kerrotaan yhdella, muuten 0
						//Rework this shit
				}
					
			}
			
			if(	keyWordClicked && spot == 1)
			{
				GUI.skin = UISkinAltSkin;
			}		
					
			if(GUI.Button(Rect(80 + previousButtonSpace + (keyWordAnimationPos*spot) ,45, wordLength * LetterSize + wordSpacing + (100*spot), 50 + (100*spot)), wordBuffer[j], "label"))
			{
			Debug.Log(wordBuffer[j]);
				if(ConversationCounter == conversationKeyWordSpot)
				{
					if(wordBuffer[j] == conversationKeyWord)
					{
						keyWordClicked = true;
						
					}
				}
			
				
				
				
			}
			
			if(	keyWordClicked)
			{
				GUI.skin = UISkin;
			}	
			
		
			previousButtonSpace = previousButtonSpace + (wordLength * LetterSize) + wordSpacing;
		}
	}
	
	if(GUI.Button(Rect(Screen.width * 3 / 4 - spacing - btnWidth, Screen.height / 4 - btnHeight - spacing, btnWidth, btnHeight), "Cont"))
	{
	 
	 
	 if(ConversationCounter != CLength -1)
	 {
	 ConversationCounter++; //Siirrytään keskustelussa eteenpäin
	 }
	 else
	 {
	 	TalkBoxOpen = false; //Sulje keskustelupalkki
	 	ConversationCounter = 0; //Keskustelu alkuun
	}
	 
	 
	
	 
	
	}
	GUI.EndGroup();
	
	
	
	
	}
	
	
				
	
}



/* Using GUIContent to display a tooltip 


// JavaScript
function OnGUI () {
	// This line feeds "This is the tooltip" into GUI.tooltip
	GUI.Button (Rect (10,10,100,20), GUIContent ("Click me", "This is the tooltip"));
	// This line reads and displays the contents of GUI.tooltip
	GUI.Label (Rect (10,40,100,20), GUI.tooltip);
}
*/


/* Using GUIContent to display an image, a string, and a tooltip 


// JavaScript
var icon : Texture2D;

function OnGUI () {
	GUI.Button (Rect (10,10,100,20), GUIContent ("Click me", icon, "This is the tooltip"));
	GUI.Label (Rect (10,40,100,20), GUI.tooltip);
}
*/



/* GUI.RepeatButton example


// JavaScript
function OnGUI () {
	if (GUI.RepeatButton (Rect (25, 25, 100, 30), "RepeatButton")) {
		// This code is executed every frame that the RepeatButton remains clicked
	}
}
*/


/* Place a Button normally   (Huomaa \n)
	if (GUILayout.RepeatButton ("Increase max\nSlider Value"))
	{
		maxSliderValue += 3.0 * Time.deltaTime;
	}
*/


//http://docs.unity3d.com/Documentation/Components/gui-Controls.html