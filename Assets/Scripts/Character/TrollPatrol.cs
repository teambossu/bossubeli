﻿using UnityEngine;
using System.Collections;

public class TrollPatrol : MonoBehaviour 
{
	
	Vector3 m_startPosition;
	public Vector3 Target = Vector3.zero;
	public float PatrolSpeed = 0;
	public float TurnRadius = 2;
    public float RotationSpeed = 10;
	
	bool m_goingToTarget = true;
	
	Vector3 m_turningTarget;
	bool m_isTurning = false;

    //values for internal use
    private Quaternion m_lookRotation;
    private Vector3 m_direction;

	// Use this for initialization
	void Start () 
	{
		m_startPosition = transform.position;
        transform.LookAt(Target);
        enabled = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
        //see if we need to rotate
        if (m_isTurning)    
        {
            //find the vector pointing from our position to the target
            m_direction = (m_turningTarget - transform.position).normalized;

            //create the rotation we need to be in to look at the target
            m_lookRotation = Quaternion.LookRotation(m_direction);

            //rotate us over time according to speed until we are in the required rotation
            transform.rotation = Quaternion.Slerp(transform.rotation, m_lookRotation, Time.deltaTime * RotationSpeed);

            //see if the turn is complete
            //when angle is close enough, snap forward vector to target and complete turning
            if (Vector3.Angle(m_direction, transform.forward) < 5 )
            {
                transform.LookAt(m_turningTarget);
                m_isTurning = false;
            }

        }
		//move the troll
		else if (Target != Vector3.zero && PatrolSpeed > 0)
		{
			//see if we need to turn around
			if ( m_goingToTarget && (Vector3.Distance( transform.position, Target) < TurnRadius))
			{
				m_goingToTarget = false;
                m_turningTarget = m_startPosition;
                m_isTurning = true;
			}
			else if ( !m_goingToTarget && (Vector3.Distance( transform.position, m_startPosition) < TurnRadius))
			{

				m_goingToTarget = true;
                m_turningTarget = Target;
                m_isTurning = true;
			}
			
			//move the troll		
			transform.Translate( PatrolSpeed * Vector3.forward * Time.deltaTime);
		}
        
	
	}
}
