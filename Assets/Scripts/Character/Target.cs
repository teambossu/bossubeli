﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {

	public GameObject follower_;
	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
    #region PROPERTIES

    public GameObject Follower
    {
        get { return follower_; }
		set { follower_ = value; }
    }

    #endregion
}