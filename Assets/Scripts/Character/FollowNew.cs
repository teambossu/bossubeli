﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowObject
{
    public Transform TargetTransform { get; set; }
    public GameObject TargetObject { get; set; }
    public bool OutOfRange { get; set; }
}

public class FollowNew : MonoBehaviour
{


    public Transform player;
    public float speed = 10.0F;
    public float chaseRange = 15.0F;

    public float playerChaseRange = 10.0F;
    public float rotationSpeed = 10.0F;

    public float forceFollowTime = 20.0F;

    private Vector3 homePoint;
    public Vector3 spawnPoint;
    public Vector3 finishPoint;
    private bool walkingHome = true;
    private bool walkingToFinish = false;
    private float distanceToTravel;
    private float walkingHomeSpeed = 30.0F;

    public bool free;
    private GameObject leader = null;
    private float timeFollowed = 0.0F;
    private Animator animator;
    private float range;
    private float oldRange;
    private System.Collections.Generic.List<FollowObject> possibleTargets;

    void Start()
    {

        //sets homepoint
        homePoint = transform.position;
        free = true;
        if (player == null)
        {
            throw new UnityException("You need to set Player for script Follow on: " + this.name);
        }

        animator = GetComponent<Animator>();
        possibleTargets = new List<FollowObject>();
        range = float.PositiveInfinity;
		
		//adds every possible followed object to possibletarget
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("EvilTroll"))
        {
            FollowObject target = new FollowObject();
            target.TargetTransform = obj.transform;
            target.TargetObject = obj;
            target.OutOfRange = true;
            Debug.Log("Added duckling target: " + obj.name);
            possibleTargets.Add(target);
        }
		//adds player to follower possibletargets
        FollowObject targetPlayer = new FollowObject();
        targetPlayer.TargetTransform = player.transform;
        targetPlayer.TargetObject = player.gameObject;
        targetPlayer.OutOfRange = true;
        Debug.Log("Added duckling target: " + player.name);
        possibleTargets.Add(targetPlayer);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log ("Leader: " + leader.tag);
        if (free && !walkingHome && !walkingToFinish)
        {
			//if there is no leader or timeFollowed is bigger than forceFollowtime
            if (leader == null || timeFollowed > forceFollowTime)
            {
                foreach (FollowObject obj in possibleTargets)
                {
                    oldRange = range;
                    range = Vector3.Distance(this.transform.position, obj.TargetTransform.position);
                    if (oldRange >= range && range <= chaseRange)
                    {
                        //if already was following, unset leaders follower
                        if (leader != null)
                        {
                            leader.GetComponent<Target>().Follower = null;
                        }
						//if leader is changing, timeFollowed = 0
						if(leader != obj.TargetObject)
						{
							timeFollowed = 0;
						
                        //sets new follower
                        leader = obj.TargetObject;
						//Debug.Log ("Leader: " + leader.tag + "TimeFollowed: " + timeFollowed);
                        //while leader has follower
                        while (leader.GetComponent<Target>().Follower != null)
                        {
                            //set leaders follower to leader
                            leader = leader.GetComponent<Target>().Follower;
                        }
						
                        //set this to leaders follower
                        leader.GetComponent<Target>().Follower = this.gameObject;
						
						break;
						}
                    }
                }
            }
			
			//if duckling has target
            if (leader != null)
            {
				//add updatetime to timefollowed
                timeFollowed += Time.deltaTime;
				
                // Calculate the distance between the follower and the leader.
                range = Vector3.Distance(this.transform.position, leader.transform.position);
				
				//if range between leader and follower is smaller or same as chaseRange
				//and range is bigger than 10, move towards target
                if (range <= chaseRange && range > 10)
                {
                    Vector3 direction = leader.transform.position - this.transform.position;
                    direction.y = 0.0f;

                    this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                                Quaternion.LookRotation(direction),
                                                                rotationSpeed * Time.deltaTime);
                    this.transform.position += this.transform.forward * speed * Time.deltaTime;

                    if (animator != null)
                    {
                        animator.SetBool("Walking", true);
                    }
                }
				//else we are too away or enough near target
				//and we are standing still
                else
                {
                    if (animator != null)
                    {
                        animator.SetBool("Walking", false);
                    }
                    return; // The follower is out of range. Do nothing.
                }
            }

        }
        //walking towards homepoint or finishPoint
        else
        {
            distanceToTravel -= walkingHomeSpeed * Time.deltaTime;
            //when duckling is near enough homepoint
            //set homepoint to position and walkingHome to false
            if (distanceToTravel < 0)
            {
                if (walkingHome)
                {
                    transform.position = homePoint;
                    walkingHome = false;
                    animator.SetBool("Walking", false);
                }
                else if (walkingToFinish)
                {
                    //duckling succesfully returned
                    transform.position = finishPoint;
                    walkingToFinish = false;
                    animator.SetBool("Walking", false);
                    GameObject.FindGameObjectWithTag("SceneDirector").GetComponent<DucklingSceneDirector>().ReturnedDuckling();
                    
                }

            }
            else
            {
                this.transform.position += this.transform.forward * walkingHomeSpeed * Time.deltaTime;
            }
        }

    }

    public void Spawn()
    {
		if(!walkingHome)
		{
        walkingHome = true;
        //set duckling position to spawnpoint
        transform.position = spawnPoint;
        //duckling looks at homePoint
        transform.LookAt(homePoint);
        //calculates distance between duckling and homePoint
        distanceToTravel = Vector3.Distance(this.transform.position, homePoint);
		}
    }

    public void GoToFinish()
    {
        free = false;
        walkingToFinish = true;
        //duckling looks at finishpoint
        transform.LookAt(finishPoint);
        //calculates distance between duckling and finishPoint
        distanceToTravel = Vector3.Distance(this.transform.position, finishPoint);
        if (leader != null)
        {
            leader.GetComponent<Target>().Follower = null;
        }
    }

    public void Cheat()
    {
        transform.position = finishPoint;
        GoToFinish();
    }
}