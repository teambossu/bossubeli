﻿using UnityEngine;
using System.Collections;

public class PickUpTeddy : MonoBehaviour {
	
	public AudioClip soundEffect;
	private CutTreePuzzleSceneDirector sceneDirector;
	// Use this for initialization
	void Start () {
		sceneDirector = GameObject.FindGameObjectWithTag("SceneDirector").GetComponent<CutTreePuzzleSceneDirector>();
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnTriggerEnter(Collider collider)
	{
		AudioSource.PlayClipAtPoint(soundEffect, this.transform.position);
        TheGame.Instance.initializeConversationState("STRE");
		DestroyObject(this);
	}
}
