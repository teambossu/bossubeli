﻿/*
 * Talkable tarjoaa ulkoista toiminnallisuutta peliobjekteille joille voi puhua. Se näyttää puhumisiconin
 * kun hahmoa osoittaa hiirellä, voidaan yhdistää ObjectLabel GUIText elementin kanssa jolloin myös nimi näkyy
 * - Tomi
 */

using UnityEngine;
using System.Collections;

namespace Talkable
{
	public enum MouseOverAction
	{
		Enter = 0,
		Exit = 1
	}
	public delegate void MouseOverEventHandler(object sender, MouseOverAction action);
	
	public class Talkable : MonoBehaviour {
		
		private Animator animator;
		public string converstationId;
		public AudioClip sound;
        public Vector3 TalkPosition;
		public Texture2D hoverCursor; // Kursori joka näytetään kun pidetään hiirtä hahmon yläpuolella.
		public Vector2 cursorAdjustment = new Vector2(15.0f, 15.0f); // Tällä voidaan muuttaa mihin kursori osoittaa
		
		public event MouseOverEventHandler mouseOverEvent; // Hiirellä osoitus eventti
		private int m_idleAnim;
		// Use this for initialization
		void Start () {
			animator = this.gameObject.GetComponent<Animator>();
			m_idleAnim = Animator.StringToHash("Base Layer.Idle");
		}
		
		// Update is called once per frame
		void Update () {
			
			if (animator != null && animator.IsInTransition(0))
			{
				animator.SetInteger("Animation", 0);
			}
		}
		
		
		// Tämä on MonoBehaviourin tarjoama rajapinta funktio jota kutsutaan kun hiiri osoittaa Collider componenttiin (esim. Box Collider)
		void OnMouseOver() {
			
			// Jos on kuuntelijoita
			if (mouseOverEvent != null) {
				// Kerrotaan kuuntelijoille että hiiri osoittaa nyt objektia
				mouseOverEvent(this, MouseOverAction.Enter);
			}
			
			// Jos on annettu vaihdettava kursori
			if (hoverCursor != null)
			{
				// Annettaan käyttöjärjestelmälle piirrettävä kursori
				Cursor.SetCursor(hoverCursor, cursorAdjustment, CursorMode.Auto);
			}
			
			// Jos painetaan hiiren vasenta näppäintä niin kerrotaan TheGame objektille.
			/*
			if (Input.GetMouseButtonDown(0) && GameManager<ConversationManager>.Instance != null)
			{
				//TODO: yksi klikki kutsuu monta kertaa <- ongelma
				Debug.Log ("Click!");
				audio.PlayOneShot(sound);
				TheGame.Instance.initializeConversationState(converstationId);
			}
             * */
	
		}
		// Tämä on MonoBehaviourin tarjoama rajapinta funktio jota kutsutaan kun hiiri ei enää osoittaa Collider componenttiin (esim. Box Collider)
		 void OnMouseExit() {
			
			// Jos on kuuntelijoita
			if (mouseOverEvent != null)
			{
				// Kerrotaan että hiiri ei enää osoita objektia
				mouseOverEvent(this, MouseOverAction.Exit);
			}
			
			// Käsketään käyttöjärjestelmän piirtää vakio kursori.
			Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
		}
        // Palauttaa Talkable-skriptiin määritellyn puhumispisteen
         public Vector3 OnClick()
         {
             return TalkPosition;
         }

         public void StartConversation()
         {
            TheGame.Instance.initializeConversationState(converstationId);
         }

	}
}