﻿using UnityEngine;
using System.Collections;

public class PigPatrol : MonoBehaviour {

    Vector3 point1;
    Vector3 point2;
    Vector3 point3;

    public float PatrolSpeed = 10;
    public float TurnRadius = 2;
    public float RotationSpeed = 10;

    int m_goingToTarget;

    Vector3 m_turningTarget;
    bool m_isTurning = false;
    private Quaternion m_lookRotation;
    private Vector3 m_direction;
    public Vector3 m_target;

    private bool m_hit = false;

	// Use this for initialization
	void Start () {

        point1 = gameObject.transform.position;
        point2 = new Vector3(38, 99.7F, 13);
        point3 = new Vector3(80, 99.7F, 13);

        m_target = point2;
        m_goingToTarget = 2;
        transform.LookAt(m_target);

        gameObject.GetComponent<Animator>().SetBool("Walking", true);
	
	}
	
	// Update is called once per frame
	void Update () {

        if (m_hit)
        {
            return;
        }

        if (m_isTurning)
        {
            GetComponent<Animator>().SetBool("Walking", false);
            //find the vector pointing from our position to the target
            m_direction = (m_turningTarget - transform.position).normalized;

            //create the rotation we need to be in to look at the target
            m_lookRotation = Quaternion.LookRotation(m_direction);

            //rotate us over time according to speed until we are in the required rotation
            transform.rotation = Quaternion.Slerp(transform.rotation, m_lookRotation, Time.deltaTime * RotationSpeed);

            //see if the turn is complete
            //when angle is close enough, snap forward vector to target and complete turning
            if (Vector3.Angle(m_direction, transform.forward) < 5)
            {
                transform.LookAt(m_turningTarget);
                m_isTurning = false;
                m_target = m_turningTarget;
                GetComponent<Animator>().SetBool("Walking", true);
            }

        }
        //move the troll
        else if (m_target != Vector3.zero && PatrolSpeed > 0)
        {
            transform.LookAt(m_target);
            //move the pig		
            transform.Translate(PatrolSpeed * Vector3.forward * Time.deltaTime);
            //see if we need to turn around
            if (Vector3.Distance(transform.position, m_target) < TurnRadius)
            {
                m_isTurning = true;
                if (m_goingToTarget == 1)
                {
                    Debug.Log("->Target2");
                    m_goingToTarget = 2;
                    m_turningTarget = point2;
                }
                else if (m_goingToTarget == 2)
                {
                    Debug.Log("->Target3");
                    m_goingToTarget = 3;
                    m_turningTarget = point3;
                }
                else
                {
                    Debug.Log("->Target1");
                    m_goingToTarget = 1;
                    m_turningTarget = point1;
                }
            }        
        }	
	}

    public void GetHit()
    {
        m_hit = true;
        gameObject.rigidbody.useGravity = true;
    }
}
