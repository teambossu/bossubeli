﻿using UnityEngine;
using System.Collections;

namespace ActionItem
{
	public enum MouseOverAction
	{
		Enter = 0,
		Exit = 1
	}
	public delegate void MouseOverEventHandler(object sender, MouseOverAction action);
	
	public class ActionItem : MonoBehaviour {
		
		public AudioClip sound;
		public Texture2D hoverCursor; // Kursori joka näytetään kun pidetään hiirtä hahmon yläpuolella.
		public Vector2 cursorAdjustment = new Vector2(10.0f, 10.0f); // Tällä voidaan muuttaa mihin kursori osoittaa
		public event MouseOverEventHandler mouseOverEvent; // Hiirellä osoitus eventti
		
		
	// This is the object to follow
	public Transform player;
	// This is the object which follows
	public Transform item;
	// This is used to store the distance between the two objects.
	private float range;
	public float wantedrange;
		// Use this for initialization
		void Start () {
		}
		
		// Update is called once per frame
		void Update () {
		}
		
		// Tämä on MonoBehaviourin tarjoama rajapinta funktio jota kutsutaan kun hiiri osoittaa Collider componenttiin (esim. Box Collider)
		void OnMouseOver() {
			
			range = Vector3.Distance( player.position,item.position );
			if( range < wantedrange) 
			{
			// Jos on kuuntelijoita
			if (mouseOverEvent != null) {
				// Kerrotaan kuuntelijoille että hiiri osoittaa nyt objektia
				mouseOverEvent(this, MouseOverAction.Enter);
			}
			
			// Jos on annettu vaihdettava kursori
			if (hoverCursor != null)
			{
				// Annettaan käyttöjärjestelmälle piirrettävä kursori
				Cursor.SetCursor(hoverCursor, cursorAdjustment, CursorMode.Auto);
			}
			
			// Jos painetaan hiiren vasenta näppäintä niin 
			
			if (Input.GetMouseButtonDown(0))
			{
				//TODO: yksi klikki kutsuu monta kertaa <- ongelma
				Debug.Log ("Click Item!");
				audio.PlayOneShot(sound);
			}
			}
		}
		// Tämä on MonoBehaviourin tarjoama rajapinta funktio jota kutsutaan kun hiiri ei enää osoittaa Collider componenttiin (esim. Box Collider)
		 void OnMouseExit() {
			
			// Jos on kuuntelijoita
			if (mouseOverEvent != null)
			{
				// Kerrotaan että hiiri ei enää osoita objektia
				mouseOverEvent(this, MouseOverAction.Exit);
			}
			
			// Käsketään käyttöjärjestelmän piirtää vakio kursori.
			Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
		}
		
	}
}