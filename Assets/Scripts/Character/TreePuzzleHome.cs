﻿using UnityEngine;
using System.Collections;

public class TreePuzzleHome : MonoBehaviour {

	private GameObject m_sceneDirector;
	// Use this for initialization
	void Start () {
		m_sceneDirector = GameObject.FindGameObjectWithTag("SceneDirector");
		if (m_sceneDirector == null)
		{
			Debug.LogError("Scene has no SceneDirector");
			throw new UnityException("Scene doesn't have CutTreePuzzleSceneDirector");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider collider)
	{        
		m_sceneDirector.GetComponent<CutTreePuzzleSceneDirector>().ReturnToStart();
	}
}
