﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {
	
	// Vakiot
	private const int MOUSE_LEFT = 0;				// Hiiren vasemman näppäimen keycode on 0
	private const string TERRAIN = "Terrain";		// Liikuttava maasto TAG:ätään Terrainiksi
	private const float ROTATION_THRESHOLD = 10.0f;	// Kuinka tarkasti GameObjectin tulee kääntyä kohteeseen
	
	// Objectin ominaisuudet (Muutetaan aivan lopuksi privateiksi)
	public float moveSpeed = 10; // GameObjectin liikkumisnopeus
	public float turnSpeed = 10;  // GameObjectin kääntymisnopeus
	
	// Privatet
	private Transform transform; // GameObjectin johon tämä skripti asetetaan muutosmatriisi.
	private Vector3 destination; // Määränpää johon GameObject on matkalla.
	private float distance;		 // Etäisyys kohteeseen jotta voidaan laskea nopeus.
	
	
	// Use this for initialization
	void Start () {
		transform = gameObject.transform;	// Otetaan gameObjectin muutosmatriisi
		destination = Vector3.zero;					// Asetetaan kohde tyhjäksi jotta ei alussa liikua
	}
	
	// Update is called once per frame
	void Update () {
		
		distance = Vector3.Distance (destination, transform.position);
		
		// Jos käyttäjä klikkaa vasemmalla hiiren painikkeella niin katsotaan osuiko hän kuljettavaan pintaan
		if (Input.GetMouseButtonDown(MOUSE_LEFT))
		{
			RaycastHit objectHit;
			Ray rayCast = Camera.main.ScreenPointToRay(Input.mousePosition);
			// Ammutaan säde kamerasta kursoriin
			if (Physics.Raycast (rayCast, out objectHit))
			{
			// Jos osui johonkin niin tarkastetaan onko se käveltävä pinta.

				if (objectHit.collider.gameObject.tag == TERRAIN)
				{
					// Jos pinta oli käveltävää niin asetetaan kohdepiste.
					destination = rayCast.GetPoint(Vector3.Distance(objectHit.point, Camera.main.transform.position));
					
					// Debug tulostusta
					Debug.Log(string.Format("LeftClick World Pos: x: {0}, y: {1}, z: {2}", 
											objectHit.point.x, 
											objectHit.point.y, 
											objectHit.point.z));
					
				}
				
			}
		}
		// Jos GameObjectilla on kohde niin liikutaan sitä kohti
		if (destination != Vector3.zero)
		{
			
			Vector3 direction = destination - transform.position;
			
			// Jos GameObject ei ole kääntyneenä kohteeseen käännetään se
			
			// Lasketaan käännöksen määrä
			Vector3 newDirection = Vector3.RotateTowards(transform.forward, direction, turnSpeed * Time.deltaTime, 0.0f);
			
			// Luodaan uusi muunnos Quaternio käännökselle.
			Quaternion newRotation = Quaternion.LookRotation(newDirection);
			Debug.Log (Vector3.Angle (transform.forward, direction));
			
			// Jos käännettävää
			if (Vector3.Angle(transform.forward, direction) > 1)
			{
				newRotation.x = 0.0f; // Ei haluta kääntää x-suunnassa
				newRotation.z = 0.0f; // Ei haluta kääntää y-suunnassa
				
				transform.rotation = newRotation; // Toteutetaan käännös
			}
			// Muuten jos liikuttavaa
			if (distance > 0.01f)
			{
				destination.y = transform.position.y; // Ei liikuteta GameObjectia y-suunnassa
				// Liikutetaan GameObjectia.
				transform.position = Vector3.MoveTowards(transform.position, destination, moveSpeed * Time.deltaTime);
			}
				
			
			
			
				

			
		}
		
	}
}
