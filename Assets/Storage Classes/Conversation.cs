﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

public class Conversation
{
	
	public Conversation(string ID, TinyXmlReader reader)
	{
		m_conv_ID = ID;
		//create the Dictionary for the conversation lines
		m_conversationLines = new Dictionary<string, ConversationLine>();
		//start reading the XML
		Debug.Log ("Conversation: Beginning initialization with ID: " + m_conv_ID);

        bool firstLine = true;
		//read the entire conversation
		while (reader.Read("Conversation"))
		{
			if(reader.isOpeningTag && reader.tagName == "ConversationLine")
			{
				//pass the reader over to the conversationline
				ConversationLine tempLine = new ConversationLine(m_conv_ID, reader);
                if (firstLine)
                {
                    m_startingLine = tempLine;
                    firstLine = false;
                }
                //add the line to the Dictionary
                m_conversationLines.Add(tempLine.LineID, tempLine);
			}
		}
		
	}

    #region PUBLIC FUNCTIONS
    /// <summary>
    /// Called by the ConversationManager to get a line from the Conversation with the LineID.
    /// </summary>
    /// <param name="lineID">The lineID of the wanted line.</param>
    /// <returns>Returns the line, or null if line is not found.</returns>
    public ConversationLine GetLine(string lineID)
    {
        ConversationLine line;
        if (!m_conversationLines.TryGetValue(lineID, out line))
        {
            Debug.Log("Conversation: WARNING: GetLine called with LineID: " + lineID + ". This lineID is not in this Conversation's lines.");
            return null;
        }
        return line;

    }
    //set the wanted line to be the conversation's starting line
    public void SetStartingLine(string lineID)
    {
        Debug.Log("Conversation: Setting starting line of " + m_conv_ID + " to be: " + lineID);
        ConversationLine line;
        if (!m_conversationLines.TryGetValue(lineID, out line))
        {
            Debug.Log("Conversation: WARNING: SetStartingLine called with LineID: " + lineID + ". This lineID is not in this Conversation's lines.");
            return;
        }
        m_startingLine = line;
    }

    public void UnlockLine(string lineID)
    {
        ConversationLine tempLine;
        if (!(m_conversationLines.TryGetValue(lineID, out tempLine)))
        {
            Debug.Log("ConversationManager: WARNING: tried to unlock non-existant line with LineID: " + lineID);
            return;
        }
        tempLine.Unlock();
    }

    public void LockLine(string lineID)
    {
        ConversationLine tempLine;
        if (!(m_conversationLines.TryGetValue(lineID, out tempLine)))
        {
            Debug.Log("ConversationManager: WARNING: tried to lock non-existant line with LineID: " + lineID);
            return;
        }
        tempLine.Lock();
    }

    public List<CWPair> ChatWheelNodes()
    {
        List<CWPair> nodes = new List<CWPair>();
        //go through all the conversationlines and find the ones that should be put into the chatwheel
        foreach (KeyValuePair<string, ConversationLine> pair in m_conversationLines)
        {
            //if the line belongs to the chatwheel and is not locked
            if (pair.Value.PrimaryChatWheelLine && !pair.Value.Locked)
            {
                //add it to the List
                nodes.Add(new CWPair(pair.Value.LineID, pair.Value.IconID));
            }
        }
        return nodes;
    }

    public void Destroy()
    {
        m_conversationLines.Clear();
    }


    #endregion

    #region PROPERTIES

    public string ID
    {
        get { return m_conv_ID; }
    }

    public ConversationLine StartingLine
    {
        get { return m_startingLine; }
    }

    #endregion

    #region MEMBER VARIABLES

    Dictionary<string, ConversationLine> m_conversationLines;
	
	private readonly string m_conv_ID;
    //initialize to be the first line of the Conversation, can be set to something else as well
    private ConversationLine m_startingLine;

    
	
	
	#endregion
	
}
