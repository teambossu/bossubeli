﻿//This is a singleton class meant only for delivering different states and message from scenes to other scenes

using UnityEngine;
using System.Collections;


public class Messenger : MonoBehaviourSingleton<Messenger>
{

    #region PUBLIC FUNCTIONS

    public void Initialize()
    {
        Debug.Log("Messenger: Initializing Messenger class");
        m_ducklingPuzzleLocked = true;
        m_treePuzzleLocked = true;

        m_ducklingPuzzleSolved = false;
        m_treePuzzleSolved = false;

        m_treePuzzleMentioned = false;

        m_duckPuzzleFirstTime = true;
        m_beachSceneFirstTime = true;
        m_realDealSceneFirstTime = true;
        m_treePuzzleFirstTime = true;
		
		m_blockTree = true;

        m_gameHasEnded = false;
    }

    #endregion

    #region MEMBER VARIABLES

    private bool m_beachSceneFirstTime;
    private bool m_realDealSceneFirstTime;
    private bool m_treePuzzleFirstTime;
    private bool m_duckPuzzleFirstTime;

    private bool m_ducklingPuzzleSolved;
    private bool m_treePuzzleSolved;

    private bool m_treePuzzleLocked;
    private bool m_ducklingPuzzleLocked;

    private bool m_treePuzzleMentioned;
	
	private bool m_blockTree;

    private bool m_gameHasEnded;
    #endregion

    #region PROPERTIES

    public bool BeachSceneFirstTime
    {
        get { return m_beachSceneFirstTime; }
        set { m_beachSceneFirstTime = value; }
    }
    public bool RealDealSceneFirstTime
    {
        get { return m_realDealSceneFirstTime; }
        set { m_realDealSceneFirstTime = value; }
    }
    public bool TreePuzzleFirstTime
    {
        get { return m_treePuzzleFirstTime; }
        set { m_treePuzzleFirstTime = value; }
    }
    public bool DuckPuzzleFirsTime
    {
        get { return m_duckPuzzleFirstTime; }
        set { m_duckPuzzleFirstTime = value; }
    }

    public bool TreePuzzleMentioned
    {
        get { return m_treePuzzleMentioned; }
        set { m_treePuzzleMentioned = value; }
    }

    public bool DucklingPuzzleLocked
    {
        get { return m_ducklingPuzzleLocked; }
        set { m_ducklingPuzzleLocked = value; }
    }

    public bool TreePuzzleLocked
    {
        get { return m_treePuzzleLocked; }
        set { m_treePuzzleLocked = value; }
    }

    public bool DucklingPuzzleSolved
    {
        get { return m_ducklingPuzzleSolved; }
        set { m_ducklingPuzzleSolved = value; }
    }

    public bool TreePuzzleSolved
    {
        get { return m_treePuzzleSolved; }
        set { m_treePuzzleSolved = value; }
    }
	
	public bool BlockTree
	{
		get { return m_blockTree; }
		set { m_blockTree = value; }
	}

    public bool GameHasEnded
    {
        get { return m_gameHasEnded; }
        set { m_gameHasEnded = value; }
    }
    #endregion

}
