﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConversationLine
{
    #region ENUMS AND CONSTS

    public const int UNDEFINED = -1;

    public enum ChatWheelType
    {
        NONE,
        PRIMARY,
        YESNO
    }

    #endregion

    // constructor
    public ConversationLine(string convID, TinyXmlReader reader)
    {
        //initializing all of the values to base values
        m_lineID = "";
        m_iconID = "";
        m_voiceID = UNDEFINED;
        m_actionTriggerID = "";
        m_chatwheelActionNext = ChatWheelType.NONE;
        m_primaryChatWheelLine = false;
        m_forceEnd = false;
        m_text = "";
        m_keywords = new List<string>();
        m_keywordTriggerID = "";
        m_locked = false;
        m_nextLineID = null;
        m_lineCameraID = "";
        m_animationID = "";

        //beginning XLM reading
        //variables to help detect faulty XML files
        bool IDset = false;
        bool IconIDset = false;
        bool VoiceIDset = false;
        bool ActionTriggerIDset = false;
        bool TextSet = false;
        bool KeywordTriggerIDset = false;
        bool PrimaryChatWheelSet = false;
        bool LockedSet = false;
        bool ChatWheelNextSet = false;
        bool NextLineIDSet = false;
        bool ForceEndSet = false;
        int VariablesSet = 0;
        bool LineCameraSet = false;
        bool AnimationSet = false;
        //read everything from inside the conversationline
        while (reader.Read("ConversationLine"))
        {
            if (reader.isOpeningTag)
            {
                switch (reader.tagName)
                {
                    case "ID":
                        if (IDset) Debug.Log("ConversationLine: WARNING: Attribute 'LineID' already set. First ID was: " + m_lineID + " Now setting ID as: " + reader.content);
                        m_lineID = convID + "-" + reader.content;
                        IDset = true;

                        //create the linecameraID from the conversation ID and the identifying part of the lineID 
                        //for example: for line: PIG_01 of conversation: CRAC the default linecameraID is CRACPIG
                        string lineCameraAppendix = reader.content.Substring(0, 3);
                        m_lineCameraID = "CAM_" + convID + lineCameraAppendix;
                        break;
                    case "IconID":
                        if (IconIDset) Debug.Log("ConversationLine: WARNING: Attribute 'IconID' already set. LineID: " + m_lineID);
                        m_iconID = reader.content;
                        IconIDset = true;
                        VariablesSet++;
                        break;
                    case "VoiceID":
                        if (VoiceIDset) Debug.Log("ConversationLine: WARNING: Attribute 'VoiceID' already set. LineID: " + m_lineID);
                        m_voiceID = Convert.ToInt32(reader.content);
                        VoiceIDset = true;
                        VariablesSet++;
                        break;
                    case "ActionTriggerID":
                        if (ActionTriggerIDset) Debug.Log("ConversationLine: WARNING: Attribute 'ActionTriggerID' already set. LineID: " + m_lineID);
                        m_actionTriggerID = reader.content;
                        ActionTriggerIDset = true;
                        VariablesSet++;
                        break;
                    case "Text":
                        if (TextSet) Debug.Log("ConversationLine: WARNING: Attribute 'Text' already set. LineID: " + m_lineID);
                        m_text = reader.content;
                        TextSet = true;
                        VariablesSet++;
                        break;
                    case "Keyword":
                        m_keywords.Add(reader.content);
                        VariablesSet++;
                        break;
                    case "KeywordTriggerID":
                        if (KeywordTriggerIDset) Debug.Log("ConversationLine: WARNING: Attribute 'KeywordTriggerID' already set. LineID: " + m_lineID);
                        m_keywordTriggerID = reader.content;
                        KeywordTriggerIDset = true;
                        VariablesSet++;
                        break;
                    case "PrimaryChatWheel":
                        if (PrimaryChatWheelSet) Debug.Log("ConversationLine: WARNING: Attribute 'PrimaryChatWheel' already set. LineID: " + m_lineID);
                        if (reader.content == "True")
                        {
                            m_primaryChatWheelLine = true;
                        }
                        //if not true, leave as false and check validity
                        else if (reader.content != "False")
                        {
                            Debug.LogError("ConversationLine: ERROR: Incorrect value of Attribute 'PrimaryChatWheel' LineID: " + m_lineID);
                        }
                        PrimaryChatWheelSet = true;
                        VariablesSet++;
                        break;
                    case "Locked":
                        if (LockedSet) Debug.Log("ConversationLine: WARNING: Attribute 'Locked' already set. LineID: " + m_lineID);
                        if (reader.content == "True")
                        {
                            m_locked = true;
                        }
                        //if not true, leave as false and check validity
                        else if (reader.content != "False")
                        {
                            Debug.LogError("ConversationLine: ERROR: Incorrect value of Attribute 'Locked' LineID: " + m_lineID);
                        }
                        LockedSet = true;
                        VariablesSet++;
                        break;
                    case "ChatWheelNext":
                        if (ChatWheelNextSet) Debug.Log("ConversationLine: WARNING: Attribute 'ChatWheelNext' set twice. LineID: " + m_lineID);
                        if (reader.content == "PRIMARY")
                        {
                            m_chatwheelActionNext = ChatWheelType.PRIMARY;
                        }
                        else if (reader.content == "YESNO")
                        {
                            m_chatwheelActionNext = ChatWheelType.YESNO;
                        }
                        //Only correct values are PRIMARY, YESNO and NONE
                        else if (reader.content != "NONE")
                        {
                            Debug.Log("ConversationLine: Incorrect value of Attribute 'ChatWheelNext' LineID: " + m_lineID);
                        }
                        ChatWheelNextSet = true;
                        VariablesSet++;
                        break;
                    case "ForceEnd":
                        if (ForceEndSet) Debug.Log("ConversationLine: WARNING: Attribute 'ForceEnd' set twice. LineID: " + m_lineID);
                        if (reader.content == "True")
                        {
                            m_forceEnd = true;
                        }
                        //if not true, leave as false and check validity
                        else if (reader.content != "False")
                        {
                            Debug.Log("ConversationLine: Incorrect value of Attribute 'ForceEnd' LineID: " + m_lineID);
                        }
                        ForceEndSet = true;
                        VariablesSet++;
                        break;
                    case "NextLineID":
                        if (NextLineIDSet) Debug.Log("ConversationLine: WARNING: Attribute 'NextLineID' set twice. LineID: " + m_lineID);
                        m_nextLineID = reader.content;
                        NextLineIDSet = true;
                        VariablesSet++;
                        break;
                    case "LineCameraID":
                        if (LineCameraSet) Debug.Log("ConversationLine: WARNING: Attribute 'LineCameraID' set twice. LineID: " + m_lineID);
                        m_lineCameraID = reader.content;
                        LineCameraSet = true;
                        VariablesSet++;
                        break;
                    case "AnimationID":
                        if (LineCameraSet) Debug.Log("ConversationLine: WARNING: Attribute 'AnimationID' set twice. LineID: " + m_lineID);
                        m_animationID = reader.content;
                        AnimationSet = true;
                        VariablesSet++;
                        break;
                }
            }
        }
        //if ForceEnd and NextLine exist at the same time, the XML is faulty
        if (ForceEnd && NextLineID != null)
        {
            Debug.LogError("ConversationManager: ERROR: Faulty ConversationLine, ForceEnd and NextLine found. LineID: "
                        + m_lineID);
        }

        Debug.Log("ConversationLine: Line created. LineID : " + m_lineID + ", Variables found: " + VariablesSet);
    }
    #region PUBLIC FUNCTIONS
    /// <summary>
    /// Checks if the attempted keyword is a correct one.
    /// </summary>
    /// <returns>
    /// KeywordActionTriggerID if keyword was found, null otherwise.
    /// </returns>
    /// <param name='keywordAttempt'>
    /// The suggested keyword.
    /// </param>
    public string keyWordMatch(string keywordAttempt)
    {
        if (m_keywordFound)
        {
            Debug.Log("ConversationLine: Keyword: " + keywordAttempt + " already found. LineID: " + m_lineID);
            return null;
        }
        foreach (string keyword in m_keywords)
        {
            if (keywordAttempt == keyword)
            {
                Debug.Log("ConversationLine: Correct keyword found. Keyword: " + keyword + " LineID: " + m_lineID + ". Returning: " + m_keywordTriggerID);
                m_keywordFound = true;
                return m_keywordTriggerID;
            }
        }
        return null;
    }
    /// <summary>
    /// Unlocks the conversationline.
    /// </summary>
    public void Unlock()
    {
        m_locked = false;
        Debug.Log("ConversationLine: Unlocked line: " + m_lineID);
    }

    public void Lock()
    {
        m_locked = true;
        Debug.Log("ConversationLine: Locked line: " + m_lineID);
    }

    #endregion

    #region PROPERTIES

    public string LineID { get { return m_lineID; } }
    public string IconID { get { return m_iconID; } }
    public int VoiceID { get { return m_voiceID; } }
    public string ActionTriggerID { get { return m_actionTriggerID; } }
    public ChatWheelType ChatWheelActionNext { get { return m_chatwheelActionNext; } }
    public bool PrimaryChatWheelLine { get { return m_primaryChatWheelLine; } }
    public bool ForceEnd { get { return m_forceEnd; } }
    public string Text { get { return m_text; } }
    public string KeywordTriggerID { get { return m_keywordTriggerID; } }
    public bool Locked { get { return m_locked; } }
    public string NextLineID { get { return m_nextLineID; } }
    public string LineCameraID { get { return m_lineCameraID; } }
    public string AnimationID { get { return m_animationID; } }

    #endregion

    #region MEMBER VARIABLES

    //necessary IDs
    private readonly string m_lineID;
    private readonly string m_iconID;
    private readonly int m_voiceID;
    private readonly string m_actionTriggerID;

    //is UI supposed to show chatwheel next?
    private readonly ChatWheelType m_chatwheelActionNext;
    //is this line one of the primary chat wheel lines
    private readonly bool m_primaryChatWheelLine;

    //is this the the last line before the conversation ends?
    private readonly bool m_forceEnd;

    //the text of the line and the keywords
    private readonly string m_text;
    private readonly List<string> m_keywords;
    //the ID of the line to be unlocked by the keyword
    private readonly string m_keywordTriggerID;
    private bool m_keywordFound = false;

    //is this line of dialogue locked?
    private bool m_locked;

    //the lineID of the next line
    private readonly string m_nextLineID;

    //the ID of the camera for this line
    private readonly string m_lineCameraID;

    //the ID of the animation
    private readonly string m_animationID;


    #endregion
}
