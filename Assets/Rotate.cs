﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
	
	private Transform transform;
	
	public float rotationSpeed;
	// Use this for initialization
	void Start () {
		transform = gameObject.transform;
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(new Vector3(0,1,0), rotationSpeed);
	}
}
