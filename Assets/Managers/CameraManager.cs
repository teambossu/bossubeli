﻿using System;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;

public class CameraManager : GameManager<CameraManager>
{

    #region PUBLIC FUNCTIONS

    public void SwitchToCamera(string cameraID)
    {
        if (cameraID == "NULL")
        {
            return;
        }
        Camera camera = null;
        m_cameras.TryGetValue(cameraID, out camera);
        if (camera == null)
        {
            Debug.LogError("CameraManager: ERROR: Camera with ID: " + cameraID + " does not exist in this scene.");
            return;
        }
        //disable all other cameras
        foreach (KeyValuePair<string, Camera> entry in m_cameras)
        {
            entry.Value.enabled = false;
        }
        //enable the correct one
        camera.enabled = true;
        m_currentCamera = cameraID;

    }


    #endregion

    #region GAMEMANAGER OVERRIDES

    protected override void onAwake()
    {
        base.initialize();
    }

    protected override void onInitialize()
    {

        m_cameras = new Dictionary<string, Camera>();
   
        Camera[] cameras = Camera.allCameras;

        //Debug.LogError("Cameras loaded. Amount: " + cameras.Length);

        foreach (Camera c in cameras)
        {
            m_cameras.Add(c.name, c);
        }

        SwitchToCamera("CAM_MAIN");
    }

    protected override void onLevelLoad(int level)
    {
        //remove old cameras and load the cameras of the new scene
        m_cameras.Clear();
        Camera[] cameras = Camera.allCameras;

        foreach (Camera c in cameras)
        {
            m_cameras.Add(c.name, c);
        }
        SwitchToCamera("CAM_MAIN");

        base.onLevelLoad(level);
    }

    protected override void onUpdate()
    {

    }
    #endregion

    #region PROPERTIES

    public string CurrentCamera
    {
        get { return m_currentCamera; }
    }

    #endregion

    #region MEMBER VARIABLES

    Dictionary<string, Camera> m_cameras;
    string m_currentCamera = "";

    #endregion
}