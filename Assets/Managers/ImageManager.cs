﻿//This class manages random actions that need to be done during the game, but are so specific that they cannot be data-driven

using System;
using System.Collections.Generic;
using UnityEngine;

public class ImageManager : GameManager<ImageManager>
{

    #region PUBLIC FUNCTIONS
    // Returns the Texture for the specified iconID
    public Texture2D getChatWheelIcon(string iconID)
    {
        Texture2D icon;
        if (!m_chatWheelIcons.TryGetValue(iconID, out icon))
        {
            return null;
        }
        else
        {
            return icon;
        }

    }

    #endregion

    #region GAMEMANAGER OVERRIDES

    protected override void onAwake()
    {


        base.initialize();
    }

    protected override void onInitialize()
    {
        loadChatWheelIcons();
    }

    protected override void onUpdate()
    {

    }
    #endregion

    #region PRIVATE FUNCTIONS

    private void loadChatWheelIcons()
    {
        m_chatWheelIcons = new Dictionary<string, Texture2D>();

        UnityEngine.Object[] icons = Resources.LoadAll("ChatWheelIcons/");

        foreach (UnityEngine.Object image in icons)
        {
            m_chatWheelIcons.Add(image.name, image as Texture2D);
            Debug.Log("ImageManager: Loaded image named: " + image.name);
        }

    }

    #endregion

    #region MEMBER VARIABLES

    Dictionary<string, Texture2D> m_chatWheelIcons;



    #endregion
}
