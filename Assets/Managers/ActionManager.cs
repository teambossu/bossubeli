﻿//This class manages random actions that need to be done during the game, but are so specific that they cannot be data-driven

using System;
using System.Collections.Generic;
using UnityEngine;

public class ActionManager : GameManager<ActionManager>
{

    #region PUBLIC FUNCTIONS

    public void doAction(string actionID)
    {
        foreach (string action in m_actionsperformed)
        {
            if (action == actionID)
            {
                Debug.Log("ActionManager: this action has already been performed: " + actionID);
                return;
            }
        }
        m_actionsperformed.Add(actionID);

        //for use
        Conversation tempConv;
        switch (actionID)
        {
            case "FadeInBeachScene":
                if (Application.loadedLevelName != "beach")
                {
                    Debug.LogError("ActionManager: ERROR: tried to execute beach scene action in another scene");
                    return;
                }
                GameObject.Find("SceneDirector").GetComponent<BeachSceneDirector>().FadeIn();
                break;
               

            case "StartDucklingScene":
                if (Application.loadedLevelName != "duckpuzzle")
                {
                    Debug.LogError("ActionManager: ERROR: tried to execute duck scene action in another scene");
                    return;
                }
                //enable all the trolls in the scene
                foreach (GameObject troll in GameObject.FindGameObjectsWithTag("EvilTroll"))
                {
                    troll.GetComponent<TrollPatrol>().enabled = true;
                }
                break;

            case "FacePigTowardsFence":
                if (Application.loadedLevelName != "duckpuzzle")
                {
                    Debug.LogError("ActionManager: ERROR: tried to execute duck scene action in another scene");
                    return;
                }
                //turn pig to look at the fence
                GameObject.FindGameObjectWithTag("Player").transform.LookAt(GameObject.FindGameObjectWithTag("Fence").transform);


                break;
            case "EndDucklingScene":
                if (Application.loadedLevelName != "duckpuzzle")
                {
                    Debug.LogError("ActionManager: ERROR: tried to execute duck scene action in another scene");
                    return;
                }

                //lock the duckling and debts topic
                tempConv = ConversationManager.Instance.getConversation("CRAC");
                //set new startingline
                tempConv.SetStartingLine("CRAC-RAC_60");
                //lock the debts line
                tempConv.LockLine("CRAC-PIG_30");
                //lock the ducklings line
                tempConv.LockLine("CRAC-PIG_50");

                
                //also change the starting line of the entrypoint monologue
                tempConv = ConversationManager.Instance.getConversation("EPDU");

                tempConv.SetStartingLine("EPDU-PIG_02");

                //move out of the duckling area and lock the scenechanger
                TheGame.Instance.changeLevel("RealDeal");
                Messenger.Instance.DucklingPuzzleSolved = true;

                break;

            case "RaccoonDoesSpeakEnglish":
                if (Application.loadedLevelName != "RealDeal")
                {
                    Debug.LogError("ActionManager: ERROR: tried to execute RealDeal scene action in another scene");
                    return;
                }
                //unlock line about debts
                tempConv = ConversationManager.Instance.getConversation("CRAC");
                //line about debts
                tempConv.UnlockLine("CRAC-PIG_30");       
                //set new startingline
                tempConv.SetStartingLine("CRAC-RAC_03");
                //lock the english line
                tempConv.LockLine("CRAC-PIG_03");

                //lock the seagull line about Raccoon not speaking english
                tempConv = ConversationManager.Instance.getConversation("CSEA");
                tempConv.LockLine("CSEA-PIG_35");
                tempConv.UnlockLine("CSEA-PIG_30");

                

                break;

            case "Tutorial":

                PopUpGUIManager.Instance.DrawPopup(0, 0, Screen.width - 50, Screen.height - 50, "tutorial");
                break;

            case "TalkedAboutDebts":
                tempConv = ConversationManager.Instance.getConversation("CRAC");

                //unlock distressed and seagull lines
                //distressed line
                tempConv.UnlockLine("CRAC-PIG_80");
                //line about seagull
                tempConv.UnlockLine("CRAC-PIG_10");
                break;
			
			  case "ReceiveDebts":
                
                //unlock lines
                tempConv = ConversationManager.Instance.getConversation("CSEA");
                //line about 
                tempConv.UnlockLine("CSEA-PIG_10");
                tempConv = ConversationManager.Instance.getConversation("CRAC");
                //set Raccoon's new starting line to be much more friendly
                tempConv.SetStartingLine("CRAC-RAC_70");
				GameObject.Find("Loot").GetComponent<Sound.PlaySound>().PlaySoundOneTime();
                ConversationGUIManager.Instance.drawTitle("Received Rabbit's debts!");
			

                break;
			
            case "UnlockDucklingScene":
                if (Application.loadedLevelName != "RealDeal")
                {
                    Debug.LogError("ActionManager: ERROR: tried to execute RealDeal scene action in another scene");
                    return;
                }
                //enable the scenechanger to the duckling scene
                GameObject.Find("RealDealSceneDirector").GetComponent<RealDealSceneDirector>().UnlockDucklingSceneChanger();
                Messenger.Instance.DucklingPuzzleLocked = false;
                //change the chatwheel duckling discussion to something else
                tempConv = ConversationManager.Instance.getConversation("CRAC");
                //Lock the old 
                tempConv.LockLine("CRAC-PIG_32");
                //unlock the new duckling line
                tempConv.UnlockLine("CRAC-PIG_50");

                break;

            case "TalkedAboutDucklings":

                tempConv = ConversationManager.Instance.getConversation("CRAC");
                //lock the distressed line
                tempConv.LockLine("CRAC-PIG_80");

                break;

            case "ForestTalkedAbout":

                tempConv = ConversationManager.Instance.getConversation("CRAC");               

                //lock the old special something line and open the new
                tempConv = ConversationManager.Instance.getConversation("CSEA");
                tempConv.LockLine("CSEA-PIG_20");
                tempConv.UnlockLine("CSEA-PIG_28");

                //change forest entry point line
                tempConv = ConversationManager.Instance.getConversation("EPTR");

                tempConv.SetStartingLine("EPTR-PIG_02");

                Messenger.Instance.TreePuzzleMentioned = true;

                break;

            case "UnlockAxeDiscussion":
              
                tempConv = ConversationManager.Instance.getConversation("CRAC");
                //Unlock the line about the axe
                tempConv.UnlockLine("CRAC-PIG_20");

                break;

            case "ChainsawReceived":

                tempConv = ConversationManager.Instance.getConversation("EPTR");

                tempConv.SetStartingLine("EPTR-PIG_03");

                tempConv = ConversationManager.Instance.getConversation("CRAC");
                //lock the axe discussion
                tempConv.LockLine("CRAC-PIG_20");

                ConversationGUIManager.Instance.drawTitle("Received Chainsaw");
				GameObject.Find("Chainsaw").GetComponent<Sound.PlaySound>().PlaySoundOneTime();
				ChainsawSwap chswp = GameObject.FindGameObjectWithTag("Player").GetComponent<ChainsawSwap>();
				if (chswp != null)
				{
					chswp.EquipChainsaw();
				}


                break;

            case "CutIntoForest":
				Messenger.Instance.BlockTree = false;
                TheGame.Instance.changeLevel("cuttreepuzzle");
                Messenger.Instance.TreePuzzleLocked = false;

                break;

            case "RespawnTrees":
                GameObject.FindGameObjectWithTag("Player").GetComponent<MovingWithNavMeshOrtho>().Stop();
                GameObject.Find("Sound-TreeGrow").GetComponent<Sound.PlaySound>().PlaySoundOneTime();

                TheGame.Instance.initializeConversationState("STRS");

                break;

            case "TeddyReceived":

                ConversationGUIManager.Instance.drawTitle("Received Teddybear");
				GameObject.Find("Loot").GetComponent<Sound.PlaySound>().PlaySoundOneTime();
                break;


            case "EndTreeScene":
                //change the teddybear topic
                tempConv = ConversationManager.Instance.getConversation("CSEA");
                //lock the old teddy line
                tempConv.LockLine("CSEA-PIG_28");
                //unlock new teddy line
                tempConv.UnlockLine("CSEA-PIG_40");                

                //move out of the duckling area and lock the scenechanger
                Messenger.Instance.TreePuzzleSolved = true;
                TheGame.Instance.changeLevel("RealDeal");
                

                //also change the starting line of the entrypoint monologue
                tempConv = ConversationManager.Instance.getConversation("EPTR");
                tempConv.SetStartingLine("EPTR-PIG_04");


                break;

            case "RaccoonDoesntSpeakEnglish":

                tempConv = ConversationManager.Instance.getConversation("CSEA");
                //unlock the new raccoon topic
                tempConv.UnlockLine("CSEA-PIG_35");
                //lock the old raccoon topic
                tempConv.LockLine("CSEA-PIG_30");
                break;

            case "TeddyCameraLaunch":

                GameObject.Find("CAM_TEDDY").GetComponent<TeddyCamera>().MoveToTeddy();

                break;

            case "TeddyCameraThere":

                TheGame.Instance.initializeConversationState("STE2");

                break;

            case "TeddyCameraGoHome":

                GameObject.Find("CAM_TEDDY").GetComponent<TeddyCamera>().MoveBack();

                break;

            case "EndGame":

                 if (Application.loadedLevelName != "beach")
                {
                    Debug.LogError("ActionManager: ERROR: tried to execute beach scene action in another scene");
                    return;
                }
                GameObject.Find("SceneDirector").GetComponent<BeachSceneDirector>().FadeOut();

                Messenger.Instance.GameHasEnded = true;

                PopUpGUIManager.Instance.DrawPopup(0, 0, Screen.width - 50, Screen.height - 50, "end");

                break;

            default:
                Debug.LogError("ActionManager: ERROR: tried to invoke non-existing actionID: " + actionID);
                break;
        }

    }

    public void doKeywordAction(string keywordTriggerID)
    {
        switch (keywordTriggerID)
        {
            default:

                Debug.LogError("ActionManager: ERROR: tried to invoke non-existing keywordTriggerID: " + keywordTriggerID);
                break;
        }
    }

    #endregion

    #region GAMEMANAGER OVERRIDES

    protected override void onAwake()
    {
        m_actionsperformed = new List<string>();
        base.initialize();
    }

    protected override void onUpdate()
    {

    }
    #endregion

    #region MEMBER VARIABLES
    //True if next line should be executed

    List<string> m_actionsperformed;

    #endregion
}
