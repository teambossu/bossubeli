﻿using UnityEngine;
using System.Collections;

public class TheGame : GameManager<TheGame>
{

    // states the game can be in
    public enum State
    {
        MAIN_MENU,
        GAMEPLAY,
        CONVERSATION,
        PAUSED
    };
    public string[] STATE_NAMES =
    {
        "MAIN_MENU",
        "GAMEPLAY",
        "CONVERSATION",
        "PAUSED"
    };
	
    #region PROPERTIES

    public State CurrentState
    {
        get { return m_currentState; }
    }
	
	public string CurrentScene
	{
		get { return m_currentScene; }
	}
	public string PreviousScene
	{
		get { return m_previousScene; }
	}

    public bool IsGameOn
    {
        get { return m_isGameOn; }
    }
    #endregion

    #region GAMEMANAGER OVERRIDES

    protected override void onAwake()
    {
        if (m_instance == null)
        {
            Debug.Log("TheGame: Creating TheGame instance");
            DontDestroyOnLoad(TheGame.Instance.gameObject);
        }
        else
        {
            Debug.Log("TheGame: Destroying duplicate TheGame instance");
            Destroy(this.gameObject);
            return;
        }
        initialize();
    }
    protected override void onInitialize()
    {
        Debug.Log("TheGame: Initializing TheGame");
        //TODO: Pitäisi aloittaa main menusta
        m_currentState = State.MAIN_MENU;
        m_previousState = 0;
		m_currentScene = "MenuScene";
		m_previousScene = "";

        if (m_managerRoot != null)
        {
            //should destroy all the managers recursively
            Object.Destroy(m_managerRoot);
        }
        createManagerRoot();
        initializeManagers();
        Messenger.Instance.Initialize();

    }

    #endregion

    #region STATE MACHINE

    public void NewGame()
    {
        initialize();

        changeLevel("Beach");
        
        m_isGameOn = true;
        m_currentState = State.GAMEPLAY;
    }
	
	public void ContinueGame()
	{
		if (m_currentState != State.MAIN_MENU)
        {
            Debug.LogError("TheGame: ERROR: trying to restore gameState from other than MAIN_MENU");
        }
        Debug.Log("TheGame: Continuing game, loading previous nonmenu scene: " + m_previousNonMenuScene);
        m_currentState = State.GAMEPLAY;
        Application.LoadLevel(m_previousNonMenuScene);
		
		
	}

    public void initializeMenuState()
    {
        if (m_currentState != State.GAMEPLAY)
        {
            Debug.LogError("TheGame: ERROR: trying to initialize Menu state from other than GAMEPLAY, State is: " + (int)State.GAMEPLAY);   
        }
        
        Debug.Log("TheGame: initializing Menu State, saving previous scene as: " + m_currentScene);
        m_previousNonMenuScene = m_currentScene;

        m_currentState = State.MAIN_MENU;
        //needed for state machine to work in editor mode
        if (Application.loadedLevelName != "MenuScene")
        {
            Application.LoadLevel("MenuScene");
        }
        
        
    }

    public void initializePauseState()
    {
        Debug.Log("TheGame: Initializing pause state");
        if (m_currentState == State.MAIN_MENU || m_currentState == State.PAUSED)
        {
            //Debug.LogError("TheGame: ERROR: trying to pause from State: " + STATE_NAMES[(int)m_currentState]);
            return;
        }
        //saving previous state
        m_previousState = m_currentState;
        //setting current state to paused
        m_currentState = State.PAUSED;

        GUIManager.Instance.changeState(State.PAUSED);
    }

    public void endPauseState()
    {
        Debug.Log("TheGame: Ending pause state");
        m_currentState = TheGame.State.GAMEPLAY;

        GUIManager.Instance.changeState(State.GAMEPLAY);
    }

    public void initializeConversationState(string conversationID)
    {
        if (m_currentState != State.GAMEPLAY && m_currentState != State.PAUSED)
        {
            Debug.LogError("TheGame: ERROR: trying to initialize a conversation from State other than GAMEPLAY");
            return;
        }
        //Change the GUI Manager's state to Conversation
        Debug.Log("TheGame: Entering State: CONVERSATION");
        GUIManager.Instance.changeState(State.CONVERSATION);

        //Initialize ConversationManager
        ConversationManager.Instance.initializeConversation(conversationID);
        m_currentState = State.CONVERSATION;
    }

    public void endConversationState()
    {
        if (m_currentState != State.CONVERSATION)
        {
            Debug.LogError("TheGame: ERROR: trying to end conversation and the State is other than CONVERSATION.");
            return;
        }
        GUIManager.Instance.changeState(State.GAMEPLAY);

        Debug.Log("TheGame: Entering State: GAMEPLAY");
        m_currentState = State.GAMEPLAY;

    }

    #endregion

    #region PUBLIC FUNCTIONS


    public void changeLevel(string scene)
    {
		Debug.Log("TheGame: Changing level from: " + m_currentScene + " to: " + scene);
        //TODO: tarkistuksia state machinen tiloista jne
		m_previousScene = m_currentScene;
		m_currentScene = scene;
        Application.LoadLevel(scene);
	
    }

    public void endGame()
    {
        m_isGameOn = false;
        Application.LoadLevel("MenuScene");
    }

    #endregion

    #region PRIVATE FUNCTIONS

    //create the manager root to be displayed in the scene
    private void createManagerRoot()
    {
        m_managerRoot = new GameObject("Managers");
        DontDestroyOnLoad(m_managerRoot);
		
		GameObject gUIManager = new GameObject("GUIManagers");
		gUIManager.AddComponent(typeof(GUIManager));
        gUIManager.transform.parent = m_managerRoot.transform;
        DontDestroyOnLoad(gUIManager);

    }

    private void initializeManagers()
    {

        GameObject conversationManager = new GameObject("ConversationManager");
        conversationManager.AddComponent(typeof(ConversationManager));
        conversationManager.transform.parent = m_managerRoot.transform;
        DontDestroyOnLoad(conversationManager);

        GameObject cameraManager = new GameObject("CameraManager");
        cameraManager.AddComponent(typeof(CameraManager));
        cameraManager.transform.parent = m_managerRoot.transform;
        DontDestroyOnLoad(cameraManager);

        GameObject actionManager = new GameObject("ActionManager");
        actionManager.AddComponent(typeof(ActionManager));
        actionManager.transform.parent = m_managerRoot.transform;
        DontDestroyOnLoad(actionManager);

        GameObject imageManager = new GameObject("ImageManager");
        imageManager.AddComponent(typeof(ImageManager));
        imageManager.transform.parent = m_managerRoot.transform;
        DontDestroyOnLoad(imageManager);

        GameObject messenger = new GameObject("Messenger");
        messenger.AddComponent(typeof(Messenger));
        DontDestroyOnLoad(messenger);

    }




    #endregion

    #region MEMBER VARIABLES

    GameObject m_managerRoot = null;
	
    State m_currentState;
    State m_previousState;
	string m_currentScene;
	string m_previousScene;
    string m_previousNonMenuScene;
    bool m_isGameOn = false;

    #endregion

}
