﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Animations
{
	public enum SeagullAnimations
	{
		Idle = 0,
		Talk = 1,
		TakeOff = 2
	};
	
	public enum RaccoonAnimations
	{
		Idle = 0,
		Talk = 1,
		Wave = 2,
		Disagree = 3,
		Facepalm = 4
	}
	
	public enum PigAnimations
	{
		Idle = 0,
		Walk = 1,
		Cut = 2,
	}
	public static class AnimationManager
	{	
		public static void SubscribeCharacter(GameObject character, string characterTag)
		{
			// Otetaan alusta ja lopusta tyhjät merkit pois
			characterTag = characterTag.Trim();
			
			if (character == null)
			{
				Debug.LogError("Failed to subscribe character to AnimationManager, null character reference.");
				return;			
			}
			
			if (string.IsNullOrEmpty(characterTag))
			{
				Debug.LogError("Failed to subscribe character to AnimationManager, null or empty character tag.");
				return;
			}
			
			if (characterTag.Length != 3)
			{
				Debug.LogError("Failed to subscribe character to AnimationManager, character tag not 3 characters.");
				return;
			}
			
			// Jos hahmoa ei vielä löydy kirjastosta lisätään se sinne.
			if (!Characters.ContainsKey(characterTag))
			{
				Characters.Add(characterTag, character);
			
			}
			else // Muuten vaihdetaan se vanhan arvon tilalle.
			{
				Characters[characterTag] = character;
			}
		}
		
		public static void ClearSubscriberList()
		{
			Characters.Clear();
		}
		
		/*
		 *  RAJAPINNAT ERI HAHMOJEN ANIMAATIOILLE
		 */
		public static void PlayAnimation(GameObject character, SeagullAnimations animation)
		{
			PlayAnimation(character, (int)animation);
		}
		
		public static void PlayAnimation(GameObject character, RaccoonAnimations animation)
		{
			PlayAnimation(character, (int)animation);
		}
		
		public static void PlayAnimation(GameObject character, PigAnimations animation)
		{
			PlayAnimation(character, (int)animation);
		}
		
		/// <summary>
		/// Käynnistää animaation jos kentästä löytyy hahmo jolla on kyseinen animaatio.
		/// </summary>
		/// <param name='animation'>
		/// Conversation XML:n animaatio tagi muotoa XXX-YYY jossa XXX = hahmon tunniste ja YYY = animaation tunniste
		/// </param>
		public static void PlayAnimation(string animation)
		{
			// Jos ei saada animaatiota ei tehdä mitään
			if (string.IsNullOrEmpty(animation))
			{
				Debug.LogError("Could not play animation, animation string was empty.");
				return;
			}
			// Otetaan hahmo ja animaatio tagi
			string[] tags = animation.Split('-');
			
			// Pitäisi olla tasan kaksi arvoa
			if (tags.Length != 2)
			{
				Debug.LogError(string.Format("Could not play animation incorrect string format. ({0})", animation));				
				return;
			}
			
			// Yritetään hakea kohde
			//Debug.Log (tags[0] + " " + tags[1]);
			
			if (!Characters.ContainsKey(tags[0]))
			{
				Debug.LogError (string.Format ("Could not play animation, character matching tag '{0}' was not found", tags[0]));
				return;
			}
			GameObject target = Characters[tags[0]];
			
			if (target == null)
			{
				Debug.LogError (string.Format ("Could not play animation, no character was found with tag {0}.", tags[0]));
				return;
			}
			
			int animationNumber = 0;
						
			// Yritetään muuttaa animaatio luvuksi
			if (!int.TryParse(tags[1], out animationNumber))
			{
				Debug.LogError("Could not play animation, animation tag is not a integer");
				return;
			}
			
			// Kutsutaan animaatiota
			PlayAnimation(target, animationNumber);		
		}
		
		// Varsinainen toteutus
		private static void PlayAnimation(GameObject character, int animation)
		{
			Animator animator = character.GetComponent<Animator>();
			if (animator != null)
			{
				animator.SetInteger("Animation", (int)animation);
			}
		}
		
		private static Dictionary<string, GameObject> Characters = new Dictionary<string, GameObject>();
	}
}
