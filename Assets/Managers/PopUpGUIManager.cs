﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopUpGUIManager : GameManager<PopUpGUIManager>
{

    //HUOM!: Tänne tarvii tehdä jonkinlaista asynkronista ajastusjuttua, esim. showText pistää jonku togglen päälle että ens updatessa aletaan näyttään sitä tekstiä tms.
    //       Muuten ConversationManager ja ConversationGUIManager rajapinnat odottavat toisiaan ikuisesti.

    GUISkin UISkin = Resources.Load("Fantasy-Colorable") as GUISkin;
    GUISkin ChatWheelSkin = Resources.Load("Fantasy-ColorableFontSize") as GUISkin;


    private int xSize = 0;
    private int ySize = 0;
    private int xStart = 0;
    private int yStart = 0;

    private string storyText = "";
    private string page1 = "";
    private string page2 = "";
    private string page3 = "";
    private string page4 = "";
    private string popUpText = "";

    private bool m_end;




    public void DrawPopup(int xStartingPosition, int yStartingPosition, int xCordSize, int yCordSize, string fileName)
    {

        xStart = xStartingPosition;
        yStart = yStartingPosition;
        xSize = xCordSize;
        ySize = yCordSize;

        loadNewText(fileName); //Lataa annetun tiedostonnimen ja asettaa sen popUpTextiin
        TheGame.Instance.initializePauseState();

    }

    #region PUBLIC FUNCTIONS
    void OnGUI()
    {
        GUI.skin = UISkin;
        if (xSize != 0)
        {
            GUI.Box(new Rect(xStart, yStart, xSize, ySize), "");
            GUI.Label(new Rect(xStart + 60, yStart + 35, xSize - 100, ySize), popUpText);

            if (GUI.Button(new Rect(xStart + xSize - 60, yStart + ySize - 60, 75, 75), "Ok"))
            {
                if (popUpText != page1 && popUpText != page2 && popUpText != page3)
                {
                    xStart = 0;
                    ySize = 0;
                    yStart = 0;
                    xSize = 0;
                    if (Messenger.Instance.GameHasEnded)
                    {
                        TheGame.Instance.endGame();
                    }
                    else
                    {
                        TheGame.Instance.endPauseState();
                    }

                    if (popUpText == page4)
                    {
                        ActionManager.Instance.doAction("FadeInBeachScene");
                    }
                }
                else if (popUpText == page1)
                {
                    popUpText = page2;
                }
                else if (popUpText == page2)
                {
                    popUpText = page3;
                }
                else if (popUpText == page3)
                {
                    popUpText = page4;
                }
            }
        }
    }





    #endregion

    #region GAMEMANAGER OVERRIDES

    protected override void onAwake()
    {
        base.initialize();
    }

    protected override void onInitialize()
    {
        //load the conversations from the conversation file



    }

    #endregion

    #region PRIVATE FUNCTIONS

    private void loadNewText(string text)
    {

        //read the starting story from resources
        TextAsset LoadedText = Resources.Load(text) as TextAsset;

        if (text == "intro")
        {
            storyText = LoadedText.text;
            string[] pages = storyText.Split('@');
            page1 = pages[0];
            page2 = pages[1];
            page3 = pages[2];
            page4 = pages[3];

            popUpText = page1;
        }
        else
        {
            popUpText = LoadedText.text;
        }



    }



    #endregion

    #region MEMBER VARIABLES



    #endregion

}
