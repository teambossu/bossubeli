﻿using UnityEngine;

public abstract class GameManager<T> : MonoBehaviourSingleton<T> where T : MonoBehaviour
{

    #region PROPERTIES

    public bool Initialized
    {
        get { return m_initialized; }
    }

    #endregion

    #region PUBLIC FUNCTIONS

    public void initialize()
    {
        Debug.Log("Initializing a manager.");
        //clean up first, if already initialized
        if (m_initialized)
        {
            cleanUp();
        }

        m_initialized = true;
        onInitialize();
    }

    public void cleanUp()
    {
        m_initialized = false;
        onCleanUp();
    }

    #endregion

    #region MONOBEHAVIOUR

    public void Awake()
    {
        onAwake();
    }

    public void Update()
    {
        //if not initialized, don't do anything
        if (!m_initialized)
        {
            return;
        }
        onUpdate();
    }

    public void FixedUpdate()
    {
        //if not initialized, don't do anything
        if (!m_initialized)
        {
            return;
        }
        onFixedUpdate();

    }

    public void LateUpdate()
    {
        //if not initialized, don't do anything
        if (!m_initialized)
        {
            return;
        }
        onLateUpdate();

    }

    public void OnLevelWasLoaded(int level)
    {
        //if not initialized, don't do anything
        if (!m_initialized)
        {
            return;
        }
        onLevelLoad(level);
    }

    #endregion

    #region VIRTUAL FUNCTIONS

    //called when the manager is being initialized
    protected virtual void onInitialize() { }

    //called when the manager is cleaned up
    protected virtual void onCleanUp() { }

    //called when Unity calls Awake 
    protected virtual void onAwake() { }

    //called when Unity calls Update
    protected virtual void onUpdate() { }

    //called when Unity calls FixedUpdate
    protected virtual void onFixedUpdate() { }

    //called when Unity calls LateUpdate
    protected virtual void onLateUpdate() { }

    //called when Unity loads a new level
    //necessary for some managers protected with DontDestroyOnLoad
    protected virtual void onLevelLoad(int level) { }


    #endregion


    #region MEMBER VARIABLES

    private bool m_initialized = false;

    #endregion

}





