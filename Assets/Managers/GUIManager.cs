﻿using UnityEngine;
using System.Collections;

public class GUIManager : GameManager<GUIManager>
{


    #region PUBLIC FUNCTIONS

    /// <summary>
    /// Changes the state of the GUIManager
    /// </summary>
    /// <param name="state">The state the GUI manager should be in.</param>
    public void changeState(TheGame.State state)
    {
        
        m_currentGUIState = state;
        //disable all GUIs
        disableGUIs();

        //activate the correct GUI
        if (state == TheGame.State.CONVERSATION)
        {
            ConversationGUIManager.Instance.enabled = true;
        }
        else if (state == TheGame.State.PAUSED)
        {
            //TODO: Pause GUI Manager
        }
        else if (state == TheGame.State.MAIN_MENU)
        {
            //TODO: Main Menu GUI Manager
        }
    }

    #endregion

    #region GAMEMANAGER OVERRIDES
    protected override void onAwake()
    {
        base.initialize();
    }

    protected override void onInitialize()
    {
        //TODO: should be initialized as Main Menu
        m_currentGUIState = TheGame.State.GAMEPLAY;

        createGUIManagers();
        //disable conversationGUIManager
        ConversationGUIManager.Instance.enabled = false;
    }

    #endregion

    #region PRIVATE FUNCTIONS

    private void createGUIManagers()
    {
        //GUI Managers
        GameObject conversationGUIManager = new GameObject("ConversationGUIManager");
        conversationGUIManager.AddComponent(typeof(ConversationGUIManager));
        conversationGUIManager.transform.parent = this.transform;
        DontDestroyOnLoad(conversationGUIManager);
		
		GameObject popUpGUIManager = new GameObject("PopUpGUIManager");
		popUpGUIManager.AddComponent(typeof(PopUpGUIManager));
		popUpGUIManager.transform.parent = this.transform;
		DontDestroyOnLoad(popUpGUIManager);
		
		

    }

    private void disableGUIs()
    {
        //TODO: Tähän pitää lisätä kaikki GUIManagerit
        ConversationGUIManager.Instance.enabled = false;
    }

    #endregion

    #region MEMBER VARIABLES

    TheGame.State m_currentGUIState;

    #endregion
}
