﻿using System;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;

public class ConversationManager : GameManager<ConversationManager>
{
    #region ENUMS AND READONLYS

    public const int UNDEFINED = -1;

    public enum CharacterName
    {
        BULLDOG,
        CHICKEN,
        PIG,
        RABBIT,
        RACCOON,
        SEAGULL
    };

    public readonly string[] CharacterConvIDs = 
	{
		"C_BUL",
		"C_CHI",
		"C_PIG",
		"C_RAB",
		"C_RAC",
		"C_SEA"
	};


    #endregion

    #region PUBLIC FUNCTIONS
    /// <summary>
    /// Initializes a conversation
    /// </summary>
    /// <param name="conversationID">The ID of the conversation that should be initialized.</param>
    public void initializeConversation(string conversationID)
    {
        //see if there is a conversation on-going
        if (m_currentConversation != null)
        {
            Debug.Log("ConversationManager: WARNING: tried to initialize a conversation with ID: " + conversationID + ". While conversation with ID: " 
                + m_currentConversation.ID + " was already initialized.");
            return;
        }
        //try to find the specified ID from the Dictionary
        if (!(m_conversations.TryGetValue(conversationID, out m_currentConversation)))
        {
            Debug.Log("ConversationManager: WARNING: tried to initialize non-existant conversation with ID: " + conversationID);
            return;
        }
        //begin the conversation
        Debug.Log("ConversationManager: Initializing conversation with ID: " + conversationID);
        m_currentConversationLine = m_currentConversation.StartingLine;

        //mark the text to be sent to the GUIManager
        m_executeNextLine = true;

    }
    /// <summary>
    /// ConversationGUIManager should call this when the user performs an action that should take him to the next conversation line.
    /// </summary>
    public void nextLine()
    {
        
        if (m_currentConversationLine == null)
        {
            Debug.LogError("ERROR: Attempt to call NextLine when currentConversationLine is null.");
            return;
        }
        //for uses in specific cases if the conversation has ended
        ConversationLine currentLine = m_currentConversationLine;

        //inspect the current line on what action to do next

        //if chatwheel would be opened
        if (m_currentConversationLine.ChatWheelActionNext != ConversationLine.ChatWheelType.NONE)
        {
            if (m_currentConversationLine.ChatWheelActionNext == ConversationLine.ChatWheelType.YESNO)
            {
                //TODO: Avaa vakioitu YES/NO Chatwheel
            }
            else if (m_currentConversationLine.ChatWheelActionNext == ConversationLine.ChatWheelType.PRIMARY)
            {
                //see if we have any options open in the primary chatwheel
                List<CWPair> chatWheelNodes = m_currentConversation.ChatWheelNodes();

                //only open the chatwheel if there are options
                if (chatWheelNodes.Count > 0)
                {
                    //marking chatwheel to be opened on next update loop
                    m_chatWheelopen = ConversationLine.ChatWheelType.PRIMARY;
                    Debug.Log("ConversationManager: Marking the chatwheel to be opened.");
                }

            }

        }
        //check if the conversation is supposed to end
        else if (m_currentConversationLine.ForceEnd)
        {
            TheGame.Instance.endConversationState();
            CameraManager.Instance.SwitchToCamera("CAM_MAIN");
            reset();
            ConversationGUIManager.Instance.conversationOver();
        }
        //check if nextline exists and we didn't open chatwheel
        if (currentLine.NextLineID != null && m_chatWheelopen == ConversationLine.ChatWheelType.NONE)
        {
            Debug.Log("NEXTLINE: " + m_currentConversationLine.NextLineID);
            executeLine(m_currentConversationLine.NextLineID);
        }
        //lastly, check if triggers exist and execute them
        if (currentLine.ActionTriggerID != "")
        {
            ActionManager.Instance.doAction(currentLine.ActionTriggerID);
        }
    }

    public void executeLine(string ID)
    {
        if (ID == "EXIT")
        {
            TheGame.Instance.endConversationState();
            CameraManager.Instance.SwitchToCamera("CAM_MAIN");
            reset();
            ConversationGUIManager.Instance.conversationOver();
			return;
        }

        string[] IDs = ID.Split('-');
        string conversationID = IDs[0];
        ConversationLine line;
        //check if the line is from the active conversation
        if (m_currentConversation.ID != conversationID)
        {
            Debug.Log("ConversationManager: WARNING: tried to execute a line that is not a part of current conversation. LineID: " + ID);
            return;
        }
        //check if the line can be found in the active conversation
        line = m_currentConversation.GetLine(ID);

        if (line == null)
        {
            Debug.Log("ConversationManager: WARNING: tried to call GetLine and got null. LineID: " + ID);
            return;
        }
        //set the line to be executed
        m_currentConversationLine = line;

        //mark next line to be executed
        m_executeNextLine = true;
    }

    /// <summary>
    /// ConversationGUIManager should call this function when player clicks on a word in the conversation.
    /// </summary>
    /// <param name="clickedWord">The word the player clicked</param>
    public bool checkKey(string clickedWord)
    {
        string tryKeyword = m_currentConversationLine.keyWordMatch(clickedWord);
        if (tryKeyword != null)
        {
            //split the ID into Conversation ID and LineID
            string[] IDs = tryKeyword.Split('-');
            //find the correct conversation
            Conversation tempConv;
            if (!(m_conversations.TryGetValue(IDs[0], out tempConv)))
            {
                Debug.Log("ConversationManager: WARNING: non-existent ConversationID: " + IDs[0] + " on KeywordTriggerID");
                return false;
            }
            else
            {
                tempConv.UnlockLine(tryKeyword);
                return true;
            }
          
        }
        return false;
    }
    //returns the pointer to the specified conversation
    public Conversation getConversation(string convID)
    {
        Conversation conv;
        if (!m_conversations.TryGetValue(convID, out conv))
        {
            Debug.Log("Conversation: WARNING: getConversation called with ID: " + convID + ". This conversation does not exist.");
            return null;
        }
        else 
        {
            return conv;
        }
    }


    #endregion

    #region GAMEMANAGER OVERRIDES

    protected override void onAwake()
    {
        base.initialize();
    }


    protected override void onInitialize()
    {

        m_conversations = new Dictionary<string, Conversation>();
        //destroy old conversations if they exist
        foreach (KeyValuePair<string, Conversation> conv in m_conversations)
        {
            conv.Value.Destroy();
        }
        m_conversations.Clear();
        //load the conversations from the conversation file
        loadConversations();

        //create the conversation root
        reset();

    }

    protected override void onUpdate()
    {
        //if next line should be executed
        if (m_executeNextLine)
        {
            //change the camera to the correct one
            if (m_currentConversationLine.LineCameraID != "N/A")
            {
                CameraManager.Instance.SwitchToCamera(m_currentConversationLine.LineCameraID);
            }

            m_executeNextLine = false;
            ConversationGUIManager.Instance.showText(m_currentConversationLine.Text);
            if (m_currentConversationLine.AnimationID != "")
            {
                Animations.AnimationManager.PlayAnimation(m_currentConversationLine.AnimationID);
            }
            
        }
        if (m_chatWheelopen != ConversationLine.ChatWheelType.NONE)
        {
            if (m_chatWheelopen == ConversationLine.ChatWheelType.PRIMARY)
            {
                List<CWPair> chatWheelNodes = m_currentConversation.ChatWheelNodes();
                Debug.Log("ConversationManager: Calling openChatWheel");
                ConversationGUIManager.Instance.openChatWheel(chatWheelNodes);
                //resetting chatwheel opening status
                m_chatWheelopen = ConversationLine.ChatWheelType.NONE;
            }
            else if (m_chatWheelopen == ConversationLine.ChatWheelType.YESNO)
            {
            }
        }

        base.onUpdate();
    }
    #endregion

    #region PRIVATE FUNCTIONS

    private void loadConversations()
    {

        //read the conversation file from Resources
        TextAsset conversations = Resources.Load("Conversations") as TextAsset;
        //string conversations = System.IO.File.ReadAllText(Application.dataPath + "/Resources/Conversations.xml");
        TinyXmlReader reader = new TinyXmlReader(conversations.text);

        while (reader.Read())
        {
            if (reader.isOpeningTag)
            {
                Debug.Log("ConversationManager: Opening XMLfile, root: " + reader.tagName);

            }
            if (reader.tagName == "Conversation")
            {
                while (reader.Read("Conversation"))
                {
                    if (reader.isOpeningTag)
                    {
                        Debug.Log("ConversationManager: Opening tag " + reader.tagName);
                    }
                    if (reader.tagName == "ID")
                    {
                        string convID = reader.content;
                        Debug.Log("ConversationManager: Found ID, " + reader.content);

                        //pass the reader over to the conversation
                        Conversation temp = new Conversation(convID, reader);
                        m_conversations.Add(convID, temp);
                    }

                }

            }
        }

    }

    private void reset()
    {
        m_currentConversationLine = null;
        m_currentConversation = null;
        m_executeNextLine = false;
        m_chatWheelopen = ConversationLine.ChatWheelType.NONE;
    }

    #endregion

    #region MEMBER VARIABLES

    Dictionary<string, Conversation> m_conversations;

    Conversation m_currentConversation;

    ConversationLine m_currentConversationLine;

    //True if next line should be executed
    bool m_executeNextLine = false;
    //True if we should open chatwheel
    ConversationLine.ChatWheelType m_chatWheelopen = ConversationLine.ChatWheelType.NONE;

    #endregion
}
