﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConversationGUIManager : GameManager<ConversationGUIManager>
{

    //HUOM!: Tänne tarvii tehdä jonkinlaista asynkronista ajastusjuttua, esim. showText pistää jonku togglen päälle että ens updatessa aletaan näyttään sitä tekstiä tms.
    //       Muuten ConversationManager ja ConversationGUIManager rajapinnat odottavat toisiaan ikuisesti.

    GUISkin UISkin = Resources.Load("Fantasy-Colorable") as GUISkin;
    GUISkin TitleSkin = Resources.Load("Fantasy-ColorableFontSize") as GUISkin;
	

	
	CWPair TLeft = new CWPair ("", "Empty");
	CWPair TRight = new CWPair("", "Empty");
	CWPair BLeft = new CWPair ("", "Empty");
	CWPair BRight = new CWPair("EXIT", "Exit");

	
	//Texture2D TopLeft = Resources.Load("Empty") as Texture2D;
	//Texture2D TopRight = Resources.Load("Empty") as Texture2D;
	//Texture2D BottomLeft = Resources.Load("Empty") as Texture2D;
	//Texture2D BottomRight = Resources.Load("Exit") as Texture2D;

    int ChatWheelButtonHeight = 23;

    //esc-napin painamine bugaa ilman tätä

    int btnWidth = 100;
    int btnHeight = 100; //
    int spacing = 10;  //spacing between buttons

    bool chatWheelOpen = false; //Onko chatwheel auki
	bool WordFound = false;		//Onko avainsana löydetty
	int WordFoundPosition = 0;  //Missä paikassa löydetty avainsana on
	int keywordResizer = 0;		//Muutetaan buttonin kokoa sen aikaa kun se on keyword
	int keywordRepositioner = 0; //Muutetaan buttonin paikkaa kun se on keyword
	
	string convTitle = "";
	bool showTitle = false;
	float titleTimer = 0.0F;
	
	

    const int wordSpacing = 10; //Space between words
    const int LetterSize = 25; //Size of 1 letter  (for dialog)  //Fontsizen pitäis olla näillä asetuksilla 40 ja font cour.   
	
	private float keyWordTimer = 0.0F;
	
	
    string ConvLine = "";

    #region PUBLIC FUNCTIONS
    void OnGUI()
    {

        GUI.skin = UISkin;
		
		if(WordFound)
		{
			GUI.skin = TitleSkin; 
			GUI.Label(new Rect(Screen.width / 2 - 300, Screen.height / 8, 1000, 150), "You found a keyword!");
			keyWordTimer -= Time.deltaTime;
			//GameObject.Find("Keyword").GetComponent<Sound.PlaySound>().PlaySoundOneTime();
			if(keyWordTimer < 0)
			{
				WordFound = false;
				WordFoundPosition = 0;
				keyWordTimer = 0.0F;
			}
			GUI.skin = UISkin;
		}
		
		if(showTitle)
		{
			GUI.skin = TitleSkin; 
			GUI.Label(new Rect(Screen.width / 2 - 300, Screen.height / 8, 1000, 150), convTitle);
			titleTimer -= Time.deltaTime;
			
			if(titleTimer < 0)
			{
				showTitle = false;
				titleTimer = 0.0F;
			}
			GUI.skin = UISkin;
		}
			
		
					
			

        if (true)
        {

            GUI.BeginGroup(new Rect(5, Screen.height * 5 / 8, Screen.width * 3 / 4, Screen.height * 5 / 8 - 5));  //Tehdään ryhmä joka kattaa 1/4 ruudun pohjasta ja
            //3/4 ruudusta vasemmasta laidasta lähtien

            GUI.Box(new Rect(0, 0, Screen.width * 3 / 4 - 10, Screen.height * 3 / 8 - 5), ""); //Saman kokoinen laatikko keskustelulle

            string[] wordbuffer = ConvLine.Split(" "[0]);
            int previousButtonSpace = 0; //Kuinka paljon edellinen sana vei tilaa.
			int line = 0; //Rivi jolla teksti on
			
            for (int i = 0; i < wordbuffer.Length; i++)
            {
                int wordLength = wordbuffer[i].Length; //Käsitellyn sanan pituus
				
				
				if (previousButtonSpace + (wordLength * LetterSize + wordSpacing) > Screen.width * 3 / 4 - 120) //Jos sana menee rivin yli
				{
					line++; //Rivinvaihto
					previousButtonSpace = 0; //Siirrytään rivin alkuun
					
				}
					
		
				if(WordFound == true)
				{
					if (WordFoundPosition == i)
					{
						GUI.skin = TitleSkin;
						keywordResizer = 100;
						keywordRepositioner = 20;
					}
				}
				
                if (GUI.Button(new Rect(60 + previousButtonSpace, 35 + 50*line - keywordRepositioner, wordLength * LetterSize + wordSpacing + keywordResizer, 50 + keywordResizer), wordbuffer[i], "label"))
                {
					Debug.Log(wordbuffer[i]);
                    if(ConversationManager.Instance.checkKey(wordbuffer[i])) //Jos löydetään avainsana
					{
						GameObject.Find("Keyword").GetComponent<Sound.PlaySound>().PlaySoundOneTime();
						WordFound = true;
						WordFoundPosition = i;
						keyWordTimer = 5.0F;
					}
                   

                }
				
				if(WordFound == true)
				{
					if (GUI.skin != UISkin)
					{
						GUI.skin = UISkin;
						keywordResizer = 0;
						keywordRepositioner = 0;
					}
				}

                previousButtonSpace = previousButtonSpace + (wordLength * LetterSize) + wordSpacing; //Sanojen pituus yhteensä ennen seuraavaa sanaa

            }

			if(!(chatWheelOpen))
			{
            	if (GUI.Button(new Rect(Screen.width * 3 / 4 - spacing - btnWidth, Screen.height * 3 / 8 + 10 - btnHeight - spacing, btnWidth, btnHeight), "Ok"))
            	{
               		ConversationManager.Instance.nextLine();
					WordFound = false;
					WordFoundPosition = 0;
					keyWordTimer = 0.0F;
					
				
            	}
			}

           

            GUI.EndGroup();

        }


        if (chatWheelOpen)
        {
            
            GUI.BeginGroup(new Rect(10 + Screen.width * 3 / 4, Screen.height * 5 / 8, Screen.width * 1 / 4 - 15, Screen.height * 5 / 8 - 5));
            GUI.Box(new Rect(0, 0, Screen.width * 1 / 4 - 5, Screen.height * 3 / 8 - 5), "");

            GUI.skin = null;
			
			


			if (GUI.Button(new Rect(30, ChatWheelButtonHeight + 5, Screen.width / 10, Screen.height / 7)
				, ImageManager.Instance.getChatWheelIcon(TLeft.iconID))) //topLeft
            {
				ConversationManager.Instance.executeLine(TLeft.lineID);
            }
            if (GUI.Button(new Rect(Screen.width / 8, ChatWheelButtonHeight + 5, Screen.width / 10, Screen.height / 7), ImageManager.Instance.getChatWheelIcon(TRight.iconID))) //topRight
            {
				ConversationManager.Instance.executeLine(TRight.lineID);
            }

            if (GUI.Button(new Rect(30, ChatWheelButtonHeight + Screen.height / 7 + 5, Screen.width / 10, Screen.height / 7), ImageManager.Instance.getChatWheelIcon(BLeft.iconID))) //bottomLeft
            {
				ConversationManager.Instance.executeLine(BLeft.lineID);
            }

            if (GUI.Button(new Rect(Screen.width / 8, ChatWheelButtonHeight + Screen.height / 7 + 5, Screen.width / 10, Screen.height / 7), ImageManager.Instance.getChatWheelIcon(BRight.iconID))) //bottomRight
            {
				ConversationManager.Instance.executeLine(BRight.lineID);
            }


            GUI.skin = UISkin;

            GUI.EndGroup();
			
			if(ConvLine != "")
 			{
				chatWheelOpen = false;
			}

            //	}
        }
    }

    public void showText(string text)
    {
        ConvLine = text;
		
    }
	
	public void drawTitle(string title)
	{
		Debug.Log("Drawtitle called with title: " + title);
		convTitle = title;
		showTitle = true;
		titleTimer = 5.0F;
		
	}


    public void openChatWheel(List<CWPair> nodes)
    {
		ConvLine = "";
		chatWheelOpen = true;
		
		//EXIT ei voi tällä hetkellä replacee chatwheelistä
		
		if(nodes.Count > 2)
		{
			BLeft = nodes[2];
			TRight = nodes[1];
			TLeft = nodes[0];
		}
		else if (nodes.Count > 1)
		{
			TRight = nodes[1];
			TLeft = nodes[0];
		}
		else if (nodes.Count > 0)
		{
			TLeft = nodes[0];
		}


    }
	
	public void conversationOver()
	{
		chatWheelOpen = false;
		
		convTitle = "";
		showTitle = false;
		titleTimer = -1.0F;
		
		BLeft.iconID = "Empty";
		BLeft.lineID = "";
		
		TRight.iconID = "Empty";
		TRight.lineID = "";
		
		TLeft.iconID = "Empty";
		TLeft.lineID = "";
	}
		
		

	
    #endregion

    #region GAMEMANAGER OVERRIDES

    protected override void onAwake()
    {
        base.initialize();
    }

    protected override void onInitialize()
    {
        //disable on initialization
        enabled = false;
      

    }

    #endregion

    #region MEMBER VARIABLES

    #endregion

}
