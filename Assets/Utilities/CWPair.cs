﻿using UnityEngine;
using System.Collections;

public class CWPair
{
    public CWPair(string lineID, string iconID)
    {
        m_lineID = lineID;
        m_iconID = iconID;
    }
    #region PROPERTIES

    public string lineID
    {
        set { m_lineID = value; }
        get { return m_lineID; }
    }

    public string iconID
    {
        set { m_iconID = value; }
        get { return m_iconID; }
    }

    #endregion


    #region MEMBER VARIABLES
    string m_lineID;
    string m_iconID;
    #endregion
}
