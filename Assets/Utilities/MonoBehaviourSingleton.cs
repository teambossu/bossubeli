﻿using UnityEngine;
using System.Collections;

//Singleton base class derived from MonoBehaviour

public class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour 
{
	public static T Instance
	{
		get
		{
			if ( m_instance == null)
			{
				m_instance = (T) FindObjectOfType(typeof(T) );
			}
			return m_instance;
		}
	}
	protected static T m_instance;
}
