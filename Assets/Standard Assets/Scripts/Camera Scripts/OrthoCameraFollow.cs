﻿using UnityEngine;
using System.Collections;

public class OrthoCameraFollow : MonoBehaviour {
	
	public GameObject target;

    //to move the sounds in the scene with the camera
    GameObject[] sounds;

    private float minX = 174;
    private float maxX = 625;

    private float minZ = 100;
    private float maxZ = 700;

	// Use this for initialization
	void Start () 
    {
        sounds = GameObject.FindGameObjectsWithTag("Sound");
	
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null)
		{
            Vector3 newPosition = target.transform.position;
            if (newPosition.x < minX) newPosition.x = minX;
            if (newPosition.x > maxX) newPosition.x = maxX;
            if (newPosition.z < minZ) newPosition.z = minZ;
            if (newPosition.z > maxZ) newPosition.z = maxZ;

            this.transform.position = newPosition;
			this.transform.position += new Vector3(0.0f, 100.0f, 0.0f);

            foreach (GameObject sound in sounds)
            {
                sound.transform.position = this.transform.position;
            }
            
		}
	}
}
