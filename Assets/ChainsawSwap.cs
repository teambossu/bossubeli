﻿using UnityEngine;
using System.Collections;

public class ChainsawSwap : MonoBehaviour {
	
	public GameObject wSaw;
	public GameObject woSaw;
	public GameObject chainsawSoundObject;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void EquipChainsaw()
	{
		Debug.Log ("Equiped");
		
		wSaw.renderer.enabled = true;
		woSaw.renderer.enabled = false;
		if (chainsawSoundObject != null)
		{
			Sound.PlaySound soundPlayer = chainsawSoundObject.GetComponent<Sound.PlaySound>();
			if (soundPlayer != null)
			{
				if (soundPlayer.clip != null)
				{
					soundPlayer.PlaySoundOneTime();
					Debug.Log("Playing sound " + soundPlayer.clip.name);
				}
			}
			
			else
			{
				Debug.LogError("No PlaySound Component was found");
			}
		}
	}
	
	public void UnequipChainsaw()
	{
		wSaw.renderer.enabled = false;
		woSaw.renderer.enabled = true;
	}
}
