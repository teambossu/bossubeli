﻿using UnityEngine;
using System.Collections;


public class MenuSceneDirector : MonoBehaviour
{

    GameObject m_continue;
    GameObject m_newGame;

    // Use this for initialization
    void Start()
    {
        //this is here just so you can start from any scene in the editor
        if (TheGame.Instance.CurrentState != TheGame.State.MAIN_MENU)
        {
            TheGame.Instance.initializeMenuState();
        }
        m_continue = GameObject.Find("Game Continue");
        m_newGame = GameObject.Find("Game New");
        m_continue.SetActive(false);
        checkContinue();
    }

    void checkContinue()
    {
        if (TheGame.Instance.IsGameOn)
        {
            m_continue.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
