﻿using UnityEngine;
using System.Collections;


public class CutTreePuzzleSceneDirector : SceneDirector
{

    private GameObject[] m_trees;


    private int m_treesCut = 0;

    // Use this for initialization
    void Start()
    {
        m_trees = GameObject.FindGameObjectsWithTag("Tree");

        base.Start();
        if (Messenger.Instance.TreePuzzleFirstTime)
        {
            Messenger.Instance.TreePuzzleFirstTime = false;
            TheGame.Instance.initializeConversationState("STE1");
        }



    }

    // Update is called once per frame
    void Update()
    {
        //Cheat
        if (Input.GetKeyDown(KeyCode.F9))
        {
            GameObject player = GameObject.Find("pig_chainsaw");
            //Vector3 solvedposition = new Vector3(50, 0, 750);
            //player.transform.position = solvedposition; //(50, 0, 750);
            TheGame.Instance.initializeConversationState("STRE");

        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (CameraManager.Instance.CurrentCamera == "CAM_MAIN")
            {
                TheGame.Instance.initializePauseState();
                CameraManager.Instance.SwitchToCamera("CAM_HELP");
            }
            else if (CameraManager.Instance.CurrentCamera == "CAM_HELP")
            {
                CameraManager.Instance.SwitchToCamera("CAM_MAIN");
                TheGame.Instance.endPauseState();
            }
        }



        base.Update();

    }

    public void TreeCut()
    {
        m_treesCut++;
    }

    public void ReturnToStart()
    {
        if (m_treesCut > 0)
        {

            foreach (GameObject tree in m_trees)
            {
                CutTreeNew script = tree.GetComponent<CutTreeNew>();
                if (script != null)
                {
                    script.ResetTree();
                }
            }
            ActionManager.Instance.doAction("RespawnTrees");
            m_treesCut = 0;

        }
    }
}
