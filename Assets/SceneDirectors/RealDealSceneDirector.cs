using UnityEngine;
using System.Collections;

public class RealDealSceneDirector : SceneDirector
{

    public Vector3 pigSPRealDeal;
    public Vector3 pigSPDuckling;
    public Vector3 pigSPTree;

    private GameObject m_ducklingSceneChanger;
    private GameObject m_treeSceneChanger;
    private GameObject m_ducklingEntryPoint;
    private GameObject m_treeEntryPoint;

	private GameObject m_blockTree;
	
    // Use this for initialization
    void Start()
    {
        m_ducklingSceneChanger = GameObject.Find("SceneChangerDuckling");
        m_treeSceneChanger = GameObject.Find("SceneChangerTree");
        m_ducklingEntryPoint = GameObject.Find("DucklingEntry");
        m_treeEntryPoint = GameObject.Find("TreeEntry");
		m_blockTree = GameObject.Find ("BlockTree");

        //lock puzzles if they are either locked or solved
        if (Messenger.Instance.DucklingPuzzleLocked || Messenger.Instance.DucklingPuzzleSolved)
        {
            LockDucklingSceneChanger();
        }
        //otherwise unlock
        else
        {
            UnlockDucklingSceneChanger();
        }
        if (Messenger.Instance.TreePuzzleLocked || Messenger.Instance.TreePuzzleSolved)
        {
            LockTreeSceneChanger();
        }
        else
        {
            UnlockTreeSceneChanger();
        }
		
		if( Messenger.Instance.BlockTree )
		{
			m_blockTree.SetActive(true);
		}
		else 
		{
			m_blockTree.SetActive (false);
		}
        //player can't interact with tree entry point unless he knows he has to go to forest
        Vector3 pigSP = GameObject.FindGameObjectWithTag("Player").transform.position;
        string prevState = TheGame.Instance.GetComponent<TheGame>().PreviousScene;

        if (prevState == "RealDeal")
        {
            pigSP = pigSPRealDeal;
        }
        else if (prevState == "duckpuzzle")
        {
            pigSP = pigSPDuckling;
        }
        else if (prevState == "cuttreepuzzle")
        {
            pigSP = pigSPTree;
        }
        GameObject.FindGameObjectWithTag("Player").transform.position = pigSP;

        base.Start();

    }
    // Update is called once per frame
    void Update()
    {
        base.Update();
    }

    public void LockDucklingSceneChanger()
    {
        m_ducklingEntryPoint.SetActive(true);
        m_ducklingSceneChanger.SetActive(false);
    }

    public void UnlockDucklingSceneChanger()
    {
        m_ducklingEntryPoint.SetActive(false);
        m_ducklingSceneChanger.SetActive(true);
    }

    public void LockTreeSceneChanger()
    {
        m_treeEntryPoint.SetActive(true);
        m_treeSceneChanger.SetActive(false);
    }

    public void UnlockTreeSceneChanger()
    {
        m_treeEntryPoint.SetActive(false);
        m_treeSceneChanger.SetActive(true);
    }
        
    public GameObject TreeSceneChanger
    {
        get { return m_treeSceneChanger; }
    }

}
