﻿using UnityEngine;
using System.Collections;


    public class DucklingSceneDirector : SceneDirector
    {


        private int m_ducklingsHome = 0;

        // Use this for initialization
        void Start()
        {
            foreach (GameObject troll in GameObject.FindGameObjectsWithTag("EvilTroll"))
            {
                troll.GetComponent<TrollPatrol>().enabled = false;
            }
            if (Messenger.Instance.DuckPuzzleFirsTime)
            {
                Messenger.Instance.DuckPuzzleFirsTime = false;
                TheGame.Instance.initializeConversationState("SDUC");
            }
            else
            {
                //enable all the trolls in the scene
                foreach (GameObject troll in GameObject.FindGameObjectsWithTag("EvilTroll"))
                {
                    troll.GetComponent<TrollPatrol>().enabled = true;
                }
            }
			base.Start();
        }

        // Update is called once per frame
        void Update()
        {
            //cheatcode for solving the puzzle is F9
            if (Input.GetKeyDown(KeyCode.F9))
            {
                GameObject[] ducks = GameObject.FindGameObjectsWithTag("Duckling");
                foreach (GameObject duck in ducks)
                {
                    duck.GetComponent<FollowNew>().Cheat();
                }
            }
            base.Update();
        }

        public void ReturnedDuckling()
        {
            m_ducklingsHome++;

            if (m_ducklingsHome == 3)
            {
                TheGame.Instance.initializeConversationState("SDUE");
            }
        }
    }
