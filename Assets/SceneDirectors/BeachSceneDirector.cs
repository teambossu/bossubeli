﻿using UnityEngine;
using System.Collections;

public class BeachSceneDirector : SceneDirector
{
	public Vector3 pigSPRealDeal;

    private GameObject m_light;
	// Use this for initialization
	public void Start () 
	{
        m_light = GameObject.Find("Directional light");
		
		if (Messenger.Instance.BeachSceneFirstTime)
        {
			PopUpGUIManager.Instance.DrawPopup(0,0, Screen.width - 50, Screen.height - 50, "intro");
            Messenger.Instance.BeachSceneFirstTime = false;
            m_light.SetActive(false);
        }
		
		Vector3 pigSP = GameObject.FindGameObjectWithTag("Player").transform.position;
		string prevState = TheGame.Instance.GetComponent<TheGame>().PreviousScene;
		
		if(prevState == "RealDeal")
		{
			pigSP = pigSPRealDeal;
		}
		GameObject.FindGameObjectWithTag("Player").transform.position = pigSP;
		
		base.Start();
		
	}
	
	// Update is called once per frame
	public void Update () 
    {
        if (Input.GetKeyDown(KeyCode.F9))
        {
            ActionManager.Instance.doAction("EndTreeScene");
        }

        if (Input.GetKeyDown(KeyCode.F10))
        {
            Debug.Log(string.Format("MovingWithNavMesh Turning to look at: x: {0}, y: {1}, z: {2}",
                                                GameObject.Find("Seagull").transform.localPosition.x,
                                                GameObject.Find("Seagull").transform.localPosition.y,
                                                GameObject.Find("Seagull").transform.localPosition.z));
            GameObject.FindGameObjectWithTag("Player").transform.LookAt(GameObject.Find("Seagull").transform);
        }
        base.Update();
	}

    public void FadeIn()
    {
        m_light.SetActive(true);
        m_light.GetComponent<LightController>().TurnOff();
        m_light.GetComponent<LightController>().FadeIn();
    }
    public void FadeOut()
    {
        m_light.GetComponent<LightController>().FadeOut();
    }
}
