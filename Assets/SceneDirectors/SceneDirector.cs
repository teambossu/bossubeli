﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterModel
{
	public string CharacterTag { get; set; }
	public GameObject Character { get; set; }
}

abstract public class SceneDirector : MonoBehaviour {
	
	public void Start()
	{
        Debug.Log("SceneDirector: Starting baseclass initialization");
		Characters = new List<CharacterModel>();
		
		foreach (GameObject character in GameObject.FindGameObjectsWithTag("Character"))
		{
            Debug.Log("Found character with name: " + character.name);
			if (character.name.Length < 3)
			{
				throw new UnityException("Character with name under 3 letters");
			}
			// Luodaan bufferi
			CharacterModel buffer = new CharacterModel
			{
				Character = character,
				CharacterTag = character.name.Substring(0, 3).ToUpper()
			};
			// Lisätään listaan
			Characters.Add(buffer);
		}
		
		foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
		{
			if (player.name.Length < 3)
			{
				throw new UnityException("Player with name under 3 letters");
			}
			
			CharacterModel buffer = new CharacterModel
			{
				Character = player,
				CharacterTag = player.name.Substring(0, 3).ToUpper ()
			};
			
			Characters.Add(buffer);
		}
		
		Debug.Log (string.Format("Found {0} characters in scene.", Characters.Count));
		SubsribeAnimations();
		
	}

    public void Update()
    {

        //pressing escape takes you to menu
        if (Input.GetKeyDown(KeyCode.Escape) && TheGame.Instance.CurrentState == TheGame.State.GAMEPLAY)
        {
            TheGame.Instance.initializeMenuState();
        }
        
    }

	private void SubsribeAnimations()
	{
		// Tyhjennetään vanha lista
		Animations.AnimationManager.ClearSubscriberList();
		
		// Annetaan animaatio kontrollerille kaikki hahmot
		foreach (CharacterModel character in Characters)
		{
			Animations.AnimationManager.SubscribeCharacter(character.Character, character.CharacterTag);	
		}
	}
		
	protected List<CharacterModel> Characters { get; set; }	


	
	
}
